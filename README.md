# [] Chào mừng bạn đến với dự án Upload fie
Hướng dẫn này dành cho phần Frontend, vui lòng xem hướng dẫn cho Backend tại [đây]().

## 1. Các công nghệ được sử dụng trong dự án
1. [Reactjs](https://reactjs.org/) v18.2.
3. [Redux](https://redux.js.org/).
4. [Redux Toolkit](https://redux-toolkit.js.org/) v1.8.
5. [Redux thunk](https://github.com/reduxjs/redux-thunk).
6. [Axios](https://github.com/axios/axios) v0.27.
7. [React Material UI](https://mui.com/) v5.9.
8. [Sass language (SCSS)](https://sass-lang.com/).
9. [Tailwind](https://tailwindcss.com/) v3.1.

## 2. Prerequired

### 2.1. Nodejs environment
+ [Nodejs](https://nodejs.org/en/) sẽ hỗ trợ trong quá trình phát triển cũng như build sản phẩm, vui lòng sử dụng Nodejs từ `v16.16.0`.
+ Mẹo: Sử dụng [nvm](https://github.com/coreybutler/nvm-windows/releases) để có thể chuyển đổi môi trường nodejs nhanh hơn.

### 2.2. IDE, TextEditor
+ Sử dụng IDE hoặc TextEditor yêu thích của bạn, khuyến khích sử dụng chung [VS code](https://code.visualstudio.com/).

### 2.3. Install extension (Hướng dẫn này được dành riêng cho VScode, các editor khác có thể sẽ khác)
+ [editorconfig](https://editorconfig.org/)(`required`): Thiết lập quy chuẩn chung về format code.
+ [ES7+ React/Redux/React-Native snippets](https://marketplace.visualstudio.com/items?itemName=dsznajder.es7-react-js-snippets)(`options`): Hỗ trợ nhắc lệnh react.
+ [Tailwind CSS IntelliSense](https://marketplace.visualstudio.com/items?itemName=bradlc.vscode-tailwindcss)(`options`): Hỗ trợ nhắc lệnh Tailwind.
+ [CSS Formatter](https://marketplace.visualstudio.com/items?itemName=aeschli.vscode-css-formatter)(`options`): Hỗ trợ format css.
+ [SCSS Formatter](https://marketplace.visualstudio.com/items?itemName=sibiraj-s.vscode-scss-formatter)(`options`): Hỗ trợ format scss.
+ [C# to TypeScript](https://marketplace.visualstudio.com/items?itemName=adrianwilczynski.csharp-to-typescript)(`options`): Hỗ trợ chuyển đổi các đối tượng C# về Typescript.
+ [Auto Rename Tag](https://marketplace.visualstudio.com/items?itemName=formulahendry.auto-rename-tag)(`options`): Hỗ trợ viết code html.


## 3. Getting started

### 3.1. Clone source

Mở terminal yêu thích của bạn và thực thi lần lượt các lệnh sau:
```bash
cd source_folder_path
git clone  http://gitlab.fpt.net/misftel/misftel-web-ui.git
cd misftel-web-ui
code .
```
### 3.2. Setup biến môi trường
Ở môi trường local, bạn cần cung cấp các biến môi trường cho phù hợp với môi trường của bạn. Biến môi trường ở local được cấu hình trong file .env.local.
```bash
#Copy file trên window
copy .env .env.local
```
Tiến hành thay đổi các config phù hợp:

+ REACT_APP_REMOTE_SERVICE_BASE_URL: Server api
+ REACT_APP_CLIENT_HOST: serve frontend
+ REACT_APP_ALLOW_LOG_HTTP: Hiển thị log trong quá trình thực thi http
### 3.3. Install package
Tại terminal ở bước 3.1 hoặc terminal của vscode, thực thi lệnh sau
```bash
# Nếu bạn sử dụng npm
npm i
# Nếu bạn sử dụng yarn
yarn install
```
### 3.4 Start dev server
Thực thi lệnh sau tại trình terminal yêu thích của bạn
```bash
#Nếu bạn sử dụng npm
npm run dev
# Nếu bạn sử dụng yarn
yarn dev
```
Wait and enjoy (^.^)

## 4. Folder structure
```bash
.
├── public                      # Chứa các file tỉnh của dự án
│   ├── images                  # Chứa các hình ảnh
│   └── ...                     # etc
├── src
│   ├── @core                   # Chứa các cấu hình chung của tất cả các dự án
│   │   ├── hooks               # Custom hook (xem thêm ở phần 5.)
│   │   ├── http                # Cấu hình http (xem thêm ở phần 5.)
│   │   ├── utils               # Các tiện ích
│   ├── layouts
│   ├── pages                   # Các trang (page) sẽ được đặt ở đây
│   ├── stores                  # Store (Store được chia theo features)
│   │   ├── uISlice             # Chứa các state lưu trữ trạng thái UI
│   │   ├── hook.tx             # Chứa các hook quản lý state(Vui lòng không edit)
│   │   ├── index.tx            # File cấu hình store
│   ├── App.scss                # App style (Vui lòng không edit)
│   ├── App.tsx                 # Root app (vui lòng không edit)
│   └── ...                     # Vui lòng không chỉnh sửa các file này
├── .editorconfig               # file cấu hình format code
├── .gitignore                  # file cấu hình các file ko up lên git
├── craco.config.js             # file cấu hình các alias (sử dụng craco)
├── postcss.config.js           # file cấu hình build css (sử dụng postcss)
├── tailwind.config.js          # file cấu hình tailwind
│   └── ...                     # etc
```

## 5. Built-in features (tính năng có sẵn)
### 5.1 Notification (Toast)
+ Descriptions: Notification là tính năng được build sẵn hỗ trợ bạn thông báo một các nhánh chóng, được sử dụng thông qua hook.
+ Example:
  ```js
  // Tại page cần sử dụng
  export default function YourPage(){
    //...

    //Khai báo noti hook
    const notification = useNotification();
    //...
    return (
      <div>
        ...
        <button onClick={()=>notification('Hello, welcome to SU18 (^.^)');}>
          Show noti
        </button>
      </div>
    )
  }
  ```
+ Usage
  + useNotification hook: trả về người dùng 1 fuction dùng để điều khiển noti
  + notification function
  ```js
  /**
  * @param message(string | React.ReactNode): Nội dung hiển thị
  * @param options: {
  *   variant(VariantType): Loại thông báo, default success
  *   autoHideDuration(number): Thời gian (seconds) tự động xóa noti, default: 3000
  * }
  * @see VariantType: "default" | "error" | "success" | "warning" | "info"
  */
  notification(message: SnackbarMessage, options?: NotificationOptions)
  ```

### 5.2 Confirm
+ Descriptions: Confirm là tính năng được build sẵn hỗ trợ bạn xác nhận trước khi thực thi 1 việc gì đó (ví dụ: xác nhận trước khi xóa, xác nhận trước khi hủy,...), được sử dụng thông qua hook.
+ Ex:
  ```js
  // Tại page cần sử dụng
  export default function YourPage(){
    //...

    //Khai báo hook
    const confirm = useConfirm();
    const deleteRow = async () => {
          let isConfirm = await confirm.show();
          if (isConfirm){
            // Thực thi các thao tác xóa
            notification('Người dùng đồng ý xóa!');
          }
          else
              notification('Người dùng hủy thao tác!', {
                  variant: 'error'
              })
      }
    //...
    return (
      <div>
        ...
        <button onClick={deleteRow}>
          Remove Icon
        </button>
      </div>
    )
  }
  ```
+ Usage
  + useConfirm hook: trả về người dùng 1 đối tượng confirm điều khiển các yêu cầu xác nhận.
  + confirm.show: Hiển thị popup xác nhận và chờ đến khi người dùng phản hồi
  ```js
  /**
  * @param options: {
  *   okText?(string): Text hiển thị trong button ok, default: Đồng ý
  *   cancelText?(string): Text hiển thị trong button cancel, default: Hủy
  *   title?(string): Title của dialog confirm, default: Xác nhận yêu cầu!
  *   content?(string | React.ReactNode): Nội dung của dialog confirm,
  *   default: Dữ liệu sẽ không thể khôi phục sau khi đồng ý, bạn có muốn tiếp tục?
  * }
  */
  confirm.show(options?: ConfirmOptions)
  ```

### 5.3 Dialog
+ Descriptions: Dialog là tính năng được build sẵn hỗ trợ xử lý các thao tác cần xử lý trong popup, được sử dụng thông qua hook.
+ Ex:
  ```js
  // Tại page cần sử dụng
  export default function YourPage(){
    //...
    const confirm = useConfirm();
    //Khai báo hook
    const dialog = useDialog();
    const addNewUser = async () => {
          dialog.open({
            handle: {
                handleCancel: async () => {
                    return await confirm.show({
                        title: 'Bạn có muốn hủy thao tác này?'
                    });
                },
                handleOk: async ()=>{
                    return new Promise<boolean>(resolve=>{
                        setTimeout(() => {
                            resolve(true);
                        }, 2000);
                    })
                }
            },
            titleOptios: {
                title: 'Thêm mới người dùng'
            },
            content: <div>
                {[...new Array(100)]
                    .map(
                        () => `Hello SU18, đầy là đoạn văn bản rất dài, dài đến mức có thể scroll.`,
                    )
                    .join('\n')}
            </div>
        });
      }
    //...
    return (
      <div>
        ...
        <button onClick={addNewUser}>
          Add User
        </button>
      </div>
    )
  }
  ```
+ Usage
  + useDialog hook: trả về người dùng 1 đối tượng Dialog điều khiển các yêu cầu xác nhận.
  + dialog.open: Mở popup
