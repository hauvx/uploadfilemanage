import React from 'react';
import { Link } from 'react-router-dom';

export default function ForbiddenPage() {
  return (
    <React.Fragment>
      <div className='w-screen h-screen flex justify-center items-start bg-[#f8f8f8]'>
        <div className="content flex items-center flex-col">
          <img className='mt-16' src="images/page-403.png" alt="Không có quyền truy cập" />
          <h1 className='mt-6'>Bạn không có quyền truy cập trang này!!!</h1>
          <Link className='mt-6' to='/'>
            Trang chủ
          </Link>
        </div>
      </div>
    </React.Fragment>
  )
}
