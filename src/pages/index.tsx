import PageTitle from '@components/PageTitle';
import HomeIcon from '@mui/icons-material/Home';
import { ImageList, ImageListItem, Paper, Typography } from '@mui/material';
import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';

export default function IndexPage() {
  useEffect(() => {
    document.title = 'FAF-Finance Report';
  }, [])


  return (
    <React.Fragment>
      <PageTitle >
        <Link
          to="/"
        >
          <HomeIcon sx={{ mr: 0.5 }} fontSize="inherit" />
          Dashboard
        </Link>
        <Typography>
          Home
        </Typography>
      </PageTitle>
      <Paper elevation={0} sx={{ width: '1200px', marginTop: 2, marginX: 'auto' }}>
        <ImageList cols={1}>
          <ImageListItem>
            <img
              style={{
                width: '100%',
                height: 'auto'
              }}
              src={`/images/welecom.jpg?fit=crop&auto=format`}
              alt='Home page'
              loading="lazy"
            />
          </ImageListItem>
        </ImageList>
      </Paper>


    </React.Fragment>
  )
}
