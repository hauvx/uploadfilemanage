import { Column } from "./type";

const columnsFileTemplate: Column[] = [
  {
    id: 'id',
    label: 'Thao tác',
    minWidth: 50,
    allowFilter: false,
    allowSort: false,
    isCustomRender: true
  },
  {
    id: 'concatenatedSegment',
    disableHide: true,
    label: 'Tên file',
    minWidth: 200,
    allowFilter: false,
    allowSort: false,
  },
  {
    id: 'template',
    disableHide: true,
    label: 'Tên template',
    minWidth: 200,
    allowFilter: false,
    allowSort: false,
  },
  {
    id: 'accountingDate',
    label: 'Ngày cập nhật',
    minWidth: 100,
    allowSort: false,
    allowFilter: false,
    filterType: 'datetime',
  },

];
export default columnsFileTemplate;
