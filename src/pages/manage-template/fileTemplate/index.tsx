import FooterNoData from "@components/FooterNoData";
import PageTitle from "@components/PageTitle";
import TableCellContent from "@components/TableCellContent";
import InvQueryComponentTwoDate from "@components/inv/QueryComponentTwoDate";
import S18Icon from "@core/components/Icon";
import useConfirm from "@core/hooks/useConfirm";
import useLoading from "@core/hooks/useLoading";
import useNotification from "@core/hooks/useNotification";
import { Sort } from "@core/models/Sort";
import handleHttpStatusCode from "@core/utils/handleHttpStatusCode";
import {
  Divider,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TablePagination,
  TableRow,
} from "@mui/material";
import fileTemplateService from "@services/manage-template/template";
import { FileTemplateInput, Filter } from "@services/manage-template/template/models/templateInput";
import { FileTemplateOutput } from "@services/manage-template/template/models/templateOutput";
import { format } from "date-fns";
import React, { useEffect, useState } from "react";
import { Link, useSearchParams } from "react-router-dom";
import TableRowDetailButton from "./Component/TableRowDetailButton";
import Template from "./Component/Template";
import UploadFileDialog from "./Component/UploadFileDialog";
import columnsFileTemplate from "./columnsFileTemplate";
import { Column } from "./type";

const EXPORT_FileTemplate_KEY = "EXPORT_FileTemplate_KEY";
const REMOVE_FileTemplate_KEY = "REMOVE_FileTemplate_KEY";

export default function FileTemplatePage() {
  const [searchParams, setSearchParams] = useSearchParams();
  const [searchCondition, setSearchCondition] = useState<FileTemplateInput | null | undefined>(undefined);
  const [isSubmit, setIsSubmit] = useState(false);
  const [totalRow, settotalRow] = useState(0);
  //#region START INPUT STATE
  const [sorts, setSorts] = useState<Sort | undefined>(undefined);
  const [filters, setFilters] = useState<Filter | undefined>(undefined);
  //Dữ liệu render ra UI
  const [data, setData] = useState<FileTemplateOutput[]>([]);
  const loading = useLoading();
  const [isShowTemplate, setIsShowTemplate] = useState(1);
  const notification = useNotification();
  //Danh sách các colum được show ra
  const [listColum, setListColum] = useState<Column[]>([]);
  const [isSuccess, setIsSuccess] = useState(false);
  const [title, setTitle] = useState("");
  const [templateName, setTemplateName] = useState("");
  const [isOpenUploadFileDialog, setIsOpenUploadFileDialog] = useState(false);
  const confirm = useConfirm();
  /**Xử lý khi người dùng show/hide colum trên table */
  const updateShowColums = () => {
    const showColumns = columnsFileTemplate.filter((col) => !col.isHide);
    setListColum(showColumns);
  };

  //#region START FETCH DATA
  /**Xử lý khi người dùng muốn tìm kiếm (khi có điều kiện search được thay đổi) */
  useEffect(() => {
    updateShowColums();
    if (typeof searchCondition === typeof undefined) return;

    (async () => {
      let param = searchParams.get("templateCode");
      if (param) {
        loading.show(REMOVE_FileTemplate_KEY);
        try {
          const { data: _data, message, code } = await fileTemplateService.get(searchCondition);
          handleHttpStatusCode(code, message);
          setData(_data.items);
          settotalRow(_data.totalCount);
          setIsSubmit(true);
        } catch ({ message }: any) {
          notification((message as string) || "Lỗi trong quá trình xử lý, vui lòng thử lại!", {
            severity: "error",
          });
        } finally {
          loading.resolve(REMOVE_FileTemplate_KEY);
        }
      }
    })();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [searchCondition]);
  //#endregion END FETCH DATA

  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);

  const handleChangePage = async (event: unknown, newPage: number) => {
    setPage(newPage);
    setSearchCondition({
      ...searchCondition,
      p: {
        ...searchCondition?.p,
        page: newPage + 1,
      },
    });
  };

  const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
    setSearchCondition({
      ...searchCondition,
      p: {
        ...searchCondition?.p,
        rowsPerPage: +event.target.value,
        page: 1,
      },
    });
  };

  useEffect(() => {
    document.title = "File Manage";
    setTitle("Template Excel");
    setTemplateName("");
    let param = searchParams.get("templateCode");
    if (param) {
      setIsShowTemplate(0);
      setTitle("File excel template");
      setTemplateName("ABC");
    } else {
      setIsShowTemplate(2);
    }
  }, [searchParams]);

  const handleBackTemplate = () => {
    setIsSubmit(false);
  };
  const handleShowTempalte = (templateCode: string) => {
    loading.show("SHOW_TEMPLATE");
    console.log('templateCode',templateCode);
    setIsShowTemplate(2);
    setSearchParams({
      templateCode: templateCode,
    });
    loading.resolve("SHOW_TEMPLATE");
  };
  const showUploadFile = () => {
    setIsOpenUploadFileDialog(true);
  };
  const onChangeFile = async (fileName: string, file: any) => {
    setIsSuccess(false);
    let param = searchParams.get("templateCode");
    if (param) {
      loading.show(REMOVE_FileTemplate_KEY);
      try {
        const formData = new FormData();
        formData.append("file", file);
        formData.append("code", fileName);
        const { data: _data, message, code } = await fileTemplateService.uploadFile(formData);
        handleHttpStatusCode(code, message);
        notification("Upload file thành công");
        setIsSuccess(true);
      } catch ({ message }: any) {
        notification((message as string) || "Lỗi trong quá trình xử lý, vui lòng thử lại!", {
          severity: "error",
        });
      } finally {
        loading.resolve(REMOVE_FileTemplate_KEY);
      }
    }
  };
  const handleDownload = () => {
    notification("Download Thành công");
  };
  const handleDelete = async () => {
    let isConfirm = await confirm.show({
      title: `Xác nhận xóa file`,
      content: `File abc sẽ được xóa, bạn có muốn tiếp tục?`,
    });
    if (isConfirm) {
      notification("Xóa Thành công");
    }
  };
  if (isShowTemplate === 2)
    return (
      <React.Fragment>
        <div className="page inv-0407-page">
          <PageTitle>
            <Link to="/inv">
              <S18Icon className="mr-2" type="outlined">
                inventory_2
              </S18Icon>
              {title}
            </Link>
          </PageTitle>

          <Template handleShowTempalte={handleShowTempalte} />
        </div>
      </React.Fragment>
    );
  if (isShowTemplate === 0)
    return (
      <React.Fragment>
        <UploadFileDialog
          isOpen={isOpenUploadFileDialog}
          setIsOpen={setIsOpenUploadFileDialog}
          onChangeFile={onChangeFile}
          isSuccess={isSuccess}
        />
        <div className="page inv-0407-page">
          <PageTitle>
            <Link to="/inv">
              <S18Icon className="mr-2" type="outlined">
                inventory_2
              </S18Icon>
              {title} <strong className="ml-1">{templateName}</strong>
            </Link>
          </PageTitle>

          <InvQueryComponentTwoDate
            handleBackTemplate={handleBackTemplate}
            searchCondition={searchCondition}
            setSearchCondition={setSearchCondition}
            handleShowTempalte={handleShowTempalte}
            showUploadFile={showUploadFile}
            setPage={setPage}
          />
          {isSubmit && (
            <Paper sx={{ width: "100%", marginTop: 2 }}>
              <div className="w-full flex items-center justify-between">
                <h6 className="table-title p-3">Kết quả tìm kiếm</h6>
                <div className="table-action mr-3 flex"></div>
              </div>
              <Divider />
              <TableContainer className="scrollbar relative" sx={{ maxHeight: "calc(100vh - 440px)" }}>
                <Table stickyHeader aria-label="sticky table" className="small text-small">
                  <TableHead>
                    <TableRow>
                      {listColum.map((column) => (
                        <TableCell
                          key={column.id}
                          align={column.align}
                          style={{ minWidth: column.minWidth, width: column.width }}
                        >
                          <TableCellContent
                            setPage={setPage}
                            sorts={sorts}
                            setSorts={setSorts}
                            searchCondition={searchCondition}
                            setSearchCondition={setSearchCondition}
                            filters={filters}
                            setFilters={setFilters}
                            column={column}
                          />
                        </TableCell>
                      ))}
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {data.length <= 0 && (
                      <FooterNoData columnsCurrencyLenght={columnsFileTemplate.length}></FooterNoData>
                    )}
                    {data.map((row: FileTemplateOutput) => {
                      return (
                        <TableRow hover role="checkbox" tabIndex={-1} key={row.id}>
                          {listColum.map((column) => {
                            let value = row[column.id];
                            if (column.id === "accountingDate")
                              value =
                                value !== null && value !== undefined ? format(new Date(value), "dd/MM/yyyy") : "N/A";

                            return (
                              <TableCell key={column.id} align={column.align}>
                                {column.isCustomRender ? (
                                  <TableRowDetailButton
                                    handleDownload={handleDownload}
                                    handleDelete={handleDelete}
                                    row={row}
                                  />
                                ) : (
                                  value
                                )}
                              </TableCell>
                            );
                          })}
                        </TableRow>
                      );
                    })}
                  </TableBody>
                </Table>
              </TableContainer>
              <TablePagination
                rowsPerPageOptions={[10, 20, 50, 100]}
                component="div"
                count={totalRow}
                rowsPerPage={rowsPerPage}
                page={page}
                onPageChange={handleChangePage}
                onRowsPerPageChange={handleChangeRowsPerPage}
              />
            </Paper>
          )}
        </div>
      </React.Fragment>
    );
  return <div></div>;
}
