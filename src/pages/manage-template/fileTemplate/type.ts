import React from "react";


export interface Column {
    id: 'id'|'concatenatedSegment'|'template'|'accountingDate';
    label: string;
    minWidth?: number;
    width?: number;
    align?: 'right' |'center';
    disableHide?: boolean,
    isHide?: boolean,
    allowFilter?: boolean,
    isCustomRender?: boolean,
    isCustomFooter?: boolean,
    allowSort?: boolean,
    filterType?: string
}

export declare type Data = {
    id: number,
    concatenatedSegment: string,
    accountingDate : string
};

