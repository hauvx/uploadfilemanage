import S18Dialog, { S18DialogAction, S18DialogActionButtonOptions, S18DialogTitle } from "@core/components/Dialog";
import S18DialogContent from "@core/components/Dialog/S18DialogContent";
import useNotification from "@core/hooks/useNotification";
import FileUploader from "@layouts/DefaultLayout/Components/fileUploader";
import { FormControl, FormHelperText, InputLabel, OutlinedInput } from "@mui/material";
import { useFormik } from "formik";
import { useEffect, useState } from "react";
import * as yup from "yup";

const validationSchema = yup.object({
  fileName: yup.string().required("Vui lòng nhập tên file").max(128, "Không cho phép nhập quá 128 ký tự"),
  hasFile: yup.string().required("Vui lòng chọn file"),
});
export default function UpdateRoleDialog({ isOpen, setIsOpen, onChangeFile, isSuccess }: any) {
  const [isWaitingLoading, setIsWaitingLoading] = useState(false);
  const notification = useNotification();
  const [file, setFile] = useState(null);
  const [name, setName] = useState("");
  const uploadFileForm = useFormik({
    initialValues: {
      fileName: "",
      hasFile: "",
    },
    validationSchema: validationSchema,
    onSubmit: async ({ fileName, hasFile }) => {
      if (!uploadFileForm.isValid) return;
      onChangeFile(fileName, file);
    },
  });

  const onchangeFile = (files: any) => {
    uploadFileForm.values.hasFile = "";
    if (files) {
      setFile(files);
      uploadFileForm.values.hasFile = "Có";
    }
    else{
      setFile(null)
    }
  };
  useEffect(() => {
    if (isSuccess) {
      uploadFileForm.resetForm();
      setName("");
    }
  }, [isSuccess]);
  const okOptions: S18DialogActionButtonOptions = {
    handle: async () => {
      uploadFileForm.values.fileName = name;
      await uploadFileForm.submitForm();
    },
    text: "Lưu",
    icon: {
      isShow: true,
      info: {
        children: "save",
        type: "outlined",
      },
    },
  };

  return (
    <S18Dialog
      size="xs"
      isOpen={isOpen}
      handleClose={() => {
        uploadFileForm.resetForm();
        setIsOpen(false);
        setName("");
      }}
    >
      <S18DialogTitle
        isWaiting={isWaitingLoading}
        handleCancel={() => {
          uploadFileForm.resetForm();
          setName("");
          setIsOpen(false);
        }}
      >
        UploadFile Template
      </S18DialogTitle>
      <S18DialogContent>
        <form className="mt-3">
          <FormControl fullWidth className="mt-6">
            <InputLabel
              htmlFor="component-outlined"
              required={true}
              error={uploadFileForm.touched.fileName && Boolean(uploadFileForm.errors.fileName)}
            >
              Tên file
            </InputLabel>
            <OutlinedInput
              onChange={(e) => setName(e.target.value)}
              placeholder=" Tên file"
              label=" Tên file"
              name="displayName"
              error={uploadFileForm.touched.fileName && Boolean(uploadFileForm.errors.fileName)}
              value={name}
            />
            {!name && (
              <FormHelperText style={{ color: "red" }}>
                {uploadFileForm.touched.fileName && uploadFileForm.errors.fileName}
              </FormHelperText>
            )}
          </FormControl>

          <FormControl fullWidth className="mt-6">
            <FileUploader onchangeFile={onchangeFile} isSuccess={isSuccess} />
            {!file && (
              <FormHelperText style={{ color: "red" }}>
                {uploadFileForm.touched.hasFile && uploadFileForm.errors.hasFile}
              </FormHelperText>
            )}
          </FormControl>
        </form>
      </S18DialogContent>
      <S18DialogAction isWaitingOk={isWaitingLoading} okOptions={okOptions}></S18DialogAction>
    </S18Dialog>
  );
}
