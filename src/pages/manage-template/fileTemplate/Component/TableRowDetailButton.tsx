import { CloudDownload, DeleteForever } from "@mui/icons-material";
import { IconButton, Tooltip } from "@mui/material";
import React from "react";
import { Data } from "../type";
export default function TableRowDetailButton({
  row,
  handleDownload,
  handleDelete,
}: {
  row: Data;
  handleDownload: any;
  handleDelete: any;
}) {
  return (
    <React.Fragment>
      <Tooltip title="Download" className="mr-[10px]">
        <IconButton onClick={handleDownload} className="text-[green]">
          <CloudDownload />
        </IconButton>
      </Tooltip>

      <Tooltip title="Xóa">
        <IconButton onClick={handleDelete} className="text-[red]">
          <DeleteForever />
        </IconButton>
      </Tooltip>
    </React.Fragment>
  );
}
