import { Box, Card, CardContent, CardMedia, Typography } from '@mui/material';

export default function Template({ handleShowTempalte }: any) {

  return (
    <div className="controls grid pt-3 grid-cols-1 md:grid-cols-3 xl:grid-cols-4 gap-4">
      {Array.from(Array(10), (e, i) => {
        return (
          <Card
            key={i}
            sx={{ display: "flex" }}
            onClick={() => handleShowTempalte("i,false")}
            className="cursor-pointer hover:bg-[#eefff8]"
          >
            <CardMedia
              component="img"
              sx={{ width: 151 }}
              image="/images/excel.jpg"
              alt="Live from space album cover"
            />
            <Box sx={{ display: "flex", flexDirection: "column" }}>
              <CardContent sx={{ flex: "1 0 auto" }}>
                <Typography component="div" variant="h5" style={{minHeight:'100px'}} className="align-middle table-cell h-[100px]">
                  Tempalte {i+1}
                  <Typography variant="subtitle1" color="text.secondary" component="div">
                    20 file
                  </Typography>
                </Typography>
              </CardContent>
            </Box>
          </Card>
        );
      })}
    </div>
  );
}
