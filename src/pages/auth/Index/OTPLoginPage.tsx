

import useLoading from '@core/hooks/useLoading';
import useNotification from '@core/hooks/useNotification';
import http from '@core/http';
import { ACCESS_TOKEN_KEY, AUTHORIZATION_KEY, REFRESH_TOKEN_KEY, TOKEN_TYPE_KEY } from '@core/utils/appConstant';
import clientStorage from '@core/utils/clientStorage';
import handleHttpStatusCode from '@core/utils/handleHttpStatusCode';
import { Button, FormControl, FormHelperText, InputLabel, OutlinedInput } from '@mui/material';
import authService from '@services/auth';
import { useAppDispatch } from '@stores/hook';
import { fetchUserAsync } from '@stores/sessionSlice';
import { useFormik } from 'formik';
import { useEffect, useState } from 'react';
import { Link, useNavigate, useSearchParams } from 'react-router-dom';

const LOGIN_OTP_LOADING_KEY = 'LOGIN_OTP_LOADING_KEY';

export default function OTPLoginPage() {
  const [session, setSession] = useState<string | null>();
  const [searchParams, setSearchParams] = useSearchParams();
  const loading = useLoading();
  const noti = useNotification();
  const dispatch = useAppDispatch();
  const navigate = useNavigate();

  const emailForm = useFormik({
    initialValues: {
      email: '',
    },
    //validationSchema: validationSchema,
    onSubmit: async ({ email }) => {
      if (!emailForm.isValid)
        return;

      loading.show(LOGIN_OTP_LOADING_KEY);
      authService
        .otp(email)
        .then(({ data, message, code }) => {
          handleHttpStatusCode(code, message);
          setSearchParams({
            'session': data
          })
        })
        .catch(({message}) => {
          noti(message ?? 'Lỗi trong quá trình xử lý, vui lòng thử lại!', {
            severity: 'error'
          });
        })
        .finally(() => {
          loading.resolve(LOGIN_OTP_LOADING_KEY);
        })
    }
  });

  const otpForm = useFormik({
    initialValues: {
      otp: '',
    },
    //validationSchema: validationSchema,
    onSubmit: async ({ otp }) => {
      if (!emailForm.isValid)
        return;

      loading.show(LOGIN_OTP_LOADING_KEY);
      authService
        .confirmOtp(session as string, otp)
        .then(({ data: { accessToken, refreshToken }, message, code }) => {
          handleHttpStatusCode(code, message);
          //SET HEADER TOKEN TO AXIOS
          http.defaults.headers.common[AUTHORIZATION_KEY] = `${TOKEN_TYPE_KEY} ${accessToken}`;

          //SAVE TOKEN TO LOCAL STORE
          clientStorage.set(ACCESS_TOKEN_KEY, accessToken);
          clientStorage.set(REFRESH_TOKEN_KEY, refreshToken);

          dispatch(fetchUserAsync()).then(() => {
            navigate('/', {
              replace: true
            });
          })

        })
        .catch(({message}) => {
          noti(message ?? 'Lỗi trong quá trình xử lý, vui lòng thử lại!', {
            severity: 'error'
          });
        })
        .finally(() => {
          loading.resolve(LOGIN_OTP_LOADING_KEY);
        })


    }
  });

  useEffect(() => {
    setSession(searchParams.get('session'));
  }, [searchParams])


  return (
    <div className='auth-page' >
      <div className="login-form">
        <div className="logo mb-8">
          <img src="/images/logo.png" alt="logo" className='h-14' />
        </div>
        <div className='flex flex-col items-center mb-8'>
          <h2 className='my-2 capitalize'>Chào mừng bạn đến với hệ thống kế toán</h2>
        </div>
        <div className="login-method">
          <h4 className='my-4'>Nhập email nhận OTP</h4>
        </div>

        {
          session ?
            (
              <form>
                <FormControl fullWidth>
                  <InputLabel htmlFor="component-outlined">OTP</InputLabel>
                  <OutlinedInput
                    onChange={otpForm.handleChange}
                    placeholder='Nhập otp...'
                    label="OTP"
                    name='otp'
                    error={otpForm.touched.otp && Boolean(otpForm.errors.otp)}
                    value={otpForm.values.otp}
                  />
                  <FormHelperText style={{ color: "red" }}>
                    {otpForm.touched.otp && otpForm.errors.otp}
                  </FormHelperText>
                </FormControl>
                <div className="login-method">
                  <Button
                    variant="outlined"
                    className='flex justify-start items-center'
                    onClick={otpForm.submitForm}
                  >
                    <span className='normal-case'>Đăng nhập</span>
                  </Button>
                </div>
              </form>
            )
            :
            (
              <form>
                <FormControl fullWidth>
                  <InputLabel htmlFor="component-outlined">Email</InputLabel>
                  <OutlinedInput
                    onChange={emailForm.handleChange}
                    placeholder='Email nhận otp...'
                    label="Email"
                    name='email'
                    error={emailForm.touched.email && Boolean(emailForm.errors.email)}
                    value={emailForm.values.email}
                  />
                  <FormHelperText style={{ color: "red" }}>
                    {emailForm.touched.email && emailForm.errors.email}
                  </FormHelperText>
                </FormControl>
                <div className="login-method">
                  <Button
                    variant="outlined"
                    className='flex justify-start items-center'
                    onClick={emailForm.submitForm}
                  >
                    <span className='normal-case'>Nhận OTP</span>
                  </Button>
                </div>

              </form>
            )
        }

        <div className='flex flex-col items-center mt-10'>
          <Link to="/auth" className='text-black'>Chọn phương thức xác thực khác</Link>
        </div>

      </div>
    </div>

  )
}
