import S18Dialog, { S18DialogActionButtonOptions, S18DialogTitle } from '@core/components/Dialog';
import S18DialogAction from '@core/components/Dialog/S18DialogAction';
import S18DialogContent from '@core/components/Dialog/S18DialogContent';
import useNotification from '@core/hooks/useNotification';
import http from '@core/http';
import { ACCESS_TOKEN_KEY, AUTHORIZATION_KEY, REFRESH_TOKEN_KEY, TOKEN_TYPE_KEY } from '@core/utils/appConstant';
import clientStorage from '@core/utils/clientStorage';
import { DialogContentText, FormControl, InputLabel, MenuItem, Select } from '@mui/material';
import authService from '@services/auth';
import { useAppDispatch } from '@stores/hook';
import { fetchUserAsync } from '@stores/sessionSlice';
import { useFormik } from 'formik';
import { useState } from 'react';
import { useNavigate, useSearchParams } from 'react-router-dom';
import * as yup from 'yup';

const validationSchema = yup.object({
  user: yup
    .string()
    .required('Vui lòng chọn người đăng nhập!')
});

const userDevs = [
  'Hauvx',
]

export default function DevLoginDialog({ isOpen, setIsOpen }: { isOpen: boolean, setIsOpen: any }) {
  const navigate = useNavigate();
  const dispatch = useAppDispatch();
  const notification = useNotification();
  const [searchParams] = useSearchParams();
  const [isWaitingLoading, setIsWaitingLoading] = useState(false)
  const userDevForm = useFormik({
    initialValues: {
      user: 'Hauvx'
    },
    validationSchema: validationSchema,
    onSubmit: async ({ user }) => {
      if (!userDevForm.isValid)
        return;

      //Get Data from form
      //Pass
      try {
        setIsWaitingLoading(true);
        // const { data: { accessToken, refreshToken }, code, message } = await authService.loginDev({ email: user })
        // if (code >= 400) {
        //   throw new Error(message);
        // }
        //SET HEADER TOKEN TO AXIOS
        // http.defaults.headers.common[AUTHORIZATION_KEY] = `${TOKEN_TYPE_KEY} ${accessToken}`;

        //SAVE TOKEN TO LOCAL STORE
        clientStorage.set(ACCESS_TOKEN_KEY, 'accessToken');
        clientStorage.set(REFRESH_TOKEN_KEY, 'refreshToken');

        await dispatch(fetchUserAsync());
        const callbackUri = searchParams.get('callbackUri');

        navigate(callbackUri ? callbackUri : '/', {
          replace: true
        });
      } catch ({ message }: any) {
        notification(message as string || 'Lỗi trong quá trình xử lý, vui lòng thử lại!', {
          severity: 'error'
        })
      }
      finally {
        setIsWaitingLoading(false);
      }

    }
  });

  const handleClose = () => {
    setIsOpen(false)
  }


  const okOptions: S18DialogActionButtonOptions = {
    handle: async () => {
      await userDevForm.submitForm();
    },
    text: 'Đăng nhập',
    icon: {
      isShow: true,
      info: {
        children: 'login',
        type: 'outlined'
      }
    }

  }

  return (
    <S18Dialog isWaiting={isWaitingLoading} size='xs' isOpen={isOpen} handleClose={handleClose}>
      <S18DialogTitle isWaiting={isWaitingLoading} handleCancel={handleClose}>
        Đăng nhập dành cho nhà phát triển
      </S18DialogTitle>

      <S18DialogContent>
        <DialogContentText>
          + Chế độ "Nhà Phát Triển" giúp cho các thành viên trong đội phát triển đăng nhập, chuyển đổi qua lại giữa các tài khoản nhanh chóng,
          điều này giúp cho việc phát triển dễ dàng cũng như nhanh chóng hơn.
        </DialogContentText>
        <DialogContentText>
          + Chế độ "Nhà Phát Triển" chỉ tồn tại trong môi trường dev.
        </DialogContentText>
        <form className='pt-6'>
          <FormControl fullWidth>
            <InputLabel id="demo-simple-select-label">Chọn nhà phát triển</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              name='user'
              value={userDevForm.values.user}
              label="Chọn nhà phát triển"
              onChange={userDevForm.handleChange}
            >
              {
                userDevs.map(user => <MenuItem key={user} value={user} >{user}</MenuItem>)
              }
            </Select>
          </FormControl>
        </form>
      </S18DialogContent>

      <S18DialogAction isWaitingOk={isWaitingLoading} okOptions={okOptions} ></S18DialogAction>
    </S18Dialog>
  )
}
