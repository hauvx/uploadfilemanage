import useLoading from '@core/hooks/useLoading';
import useNotification from '@core/hooks/useNotification';
import clientStorage from '@core/utils/clientStorage';
import handleHttpStatusCode from '@core/utils/handleHttpStatusCode';
import CodeOffIcon from '@mui/icons-material/CodeOff';
import NavigateNextIcon from '@mui/icons-material/NavigateNext';
import { Button, ButtonGroup, FormControl, FormHelperText, TextField } from '@mui/material';
import userService from '@services/user';
import { useAppSelector } from '@stores/hook';
import { useFormik } from 'formik';
import moment from 'moment';
import pkceChallenge from 'pkce-challenge';
import { useEffect, useMemo, useState } from 'react';
import { useSearchParams } from 'react-router-dom';
import * as yup from 'yup';
import './index.scss';
import DevLoginDialog from './Index/DevLoginDialog';
import { ACCESS_TOKEN_KEY } from '@core/utils/appConstant';
const LOGIN_LOADING_KEY = 'LOGIN_LOADING_KEY';

const validationSchema = yup.object({
  //eslint-disable-next-line
  email: yup.string()
    .trim()
    // eslint-disable-next-line
    // .matches(/^[\w-\.]+\@(fpt.com.vn)/gi, 'Email sai định dạng')
    .required('Địa chỉ email không được để trống!')
})
export default function AuthPage() {
  const appConfig = useAppSelector(state => state.ui.appConfig);
  const [searchParams] = useSearchParams();
  const callbackUri = searchParams.get('callbackUri');
  const [isOpenDialogDev, setIsOpenDialogDev] = useState(false);
  const loading = useLoading();
  const noti = useNotification();

  const azureQADEedirectUri = useMemo(() => {
    const pkceChallengeInfo = pkceChallenge();
    const state = `${Math.floor(Math.random() * (1000 - 99))}-${moment().format('QyMDdewhmsSS')}`;
    clientStorage.set(`state.${state}`, JSON.stringify({
      code_challenge: pkceChallengeInfo.code_challenge,
      code_verifier: pkceChallengeInfo.code_verifier,
      callbackUri: callbackUri
    }));
    const redirect_uri = encodeURIComponent(`http://localhost:3000/login`)
    return `https://login.microsoftonline.com/36ca0248-4825-42b1-bea6-f9ba9bbbba3a/oauth2/v2.0/authorize?` +
      `client_id=1ca954a9-766c-4e15-9851-60942c12ce1b` +
      `&response_type=code` +
      `&redirect_uri=${redirect_uri}` +
      `&response_mode=query` +
      `&code_challenge_method=S256` +
      `&code_challenge=${pkceChallengeInfo.code_challenge}` +
      `&state=${state}` +
      `&scope=openid email profile`;
  },
    [appConfig, callbackUri]
  )

  const emailForm = useFormik({
    initialValues: {
      email: '',
    },
    validationSchema,
    onSubmit: async ({ email }) => {
      if (!emailForm.isValid)
        return;

      loading.show(LOGIN_LOADING_KEY);
      userService
        .existByEmailOrUsername(email)
        .then(({ data, code, message }) => {
          handleHttpStatusCode(code, message);
          if (data)
            handleAzureLoginBtn();
          else {
            noti('Người dùng không tồn tại trong hệ thống, vui lòng liên hệ admin!', {
              severity: 'error'
            });
          }
        })
        .catch(({ message }) => {
          noti(message || 'Lỗi trong quá trình xử lý, vui lòng thử lại!', {
            severity: 'error'
          });
        })
        .finally(() => {
          loading.resolve(LOGIN_LOADING_KEY);
        })
    }
  })

  const handleDevBtn = () => {
    setIsOpenDialogDev(true);
  }

  const handleAzureLoginBtn = () => {
    window.location.href = azureQADEedirectUri;
  }
  useEffect(() => {
    const accessToken = clientStorage.get(ACCESS_TOKEN_KEY);
    if (accessToken) {

    }
  }, [])
  return (
    <section className="auth-page">
      <div className="px-6 h-full text-gray-800">
        <div className="boxAuth flex xl:justify-center lg:justify-between justify-center items-center flex-wrap g-6">
          <div className="grow-0 shrink-1 md:shrink-0 basis-auto xl:w-6/12 lg:w-6/12 md:w-9/12 mb-12 md:mb-0" >
            <img src="/images/illustration_dashboard.png" className="w-full" alt="Dashboard" />
          </div>
          <div className="xl:ml-20 xl:w-5/12 lg:w-5/12 md:w-8/12 mb-12 md:mb-0">

              <form
                onSubmit={(e) => { e.preventDefault(); emailForm.submitForm() }}
                noValidate
              >
                <div className="flex flex-row items-center justify-center lg:justify-start mb-3">
                  {/* <img className="w-40" src="/images/logo-vingroup.png" alt="logo" /> */}
                </div>
                <div className="flex flex-row items-center justify-center lg:justify-start mb-3">
                  <p className="text-md">Đăng nhập</p>
                </div>
                <div className="flex flex-row items-center justify-center lg:justify-start mb-3">
                  <FormControl fullWidth variant='outlined' >
                    <TextField
                      error={emailForm.touched.email && Boolean(emailForm.errors.email)}
                      onChange={emailForm.handleChange}
                      value={emailForm.values.email}
                      label="Email"
                      placeholder='Email'
                      required
                      name='email'
                    />
                    <FormHelperText style={{ color: "red" }}>
                      {emailForm.touched.email && emailForm.errors.email}
                    </FormHelperText>
                  </FormControl>
                </div>
                <div className="text-center lg:text-left">
                  <Button type="submit" variant="contained" className='flex justify-start items-center'>
                    <NavigateNextIcon className='h-8' />
                    <span className='normal-case'>ĐĂNG NHẬP</span>
                  </Button>
                </div>
                {/* <ButtonGroup orientation='vertical' className='mt-3'>

                    <Button onClick={handleDevBtn} variant="outlined" className='flex justify-start items-center text-blue-600'>
                      <CodeOffIcon className='mr-2 h-8 w-8' />
                      <span className='normal-case'>Chế độ nhà phát triển</span>
                    </Button>
                </ButtonGroup> */}
              </form>
          </div>
        </div>
      </div>
      <DevLoginDialog isOpen={isOpenDialogDev} setIsOpen={setIsOpenDialogDev} />
    </section>


  )
}
