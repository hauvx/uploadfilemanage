import ProtectedRoute from '@core/components/ProtectedRoute '
import { Route, Routes } from 'react-router-dom'
import AuthPage from '.'
import OTPLoginPage from './Index/OTPLoginPage'

export default function authRouter() {
  return (
    <Routes>
      <Route index element={<ProtectedRoute children={<AuthPage />} />} />
      <Route path='otp' element={<OTPLoginPage  />} />
    </Routes>
  )
}
