import LoadingComponent from '@components/Loading';
import useNotification from '@core/hooks/useNotification';
import http from '@core/http';
import { ACCESS_TOKEN_KEY, AUTHORIZATION_KEY, REFRESH_TOKEN_KEY, TOKEN_TYPE_KEY, USER_NAME } from '@core/utils/appConstant';
import clientStorage from '@core/utils/clientStorage';
import handleHttpStatusCode from '@core/utils/handleHttpStatusCode';
import authService from '@services/auth';
import { useAppDispatch } from '@stores/hook';
import { fetchUserAsync, setSession } from '@stores/sessionSlice';
import { useEffect } from 'react';
import { useNavigate, useSearchParams } from 'react-router-dom';

export default function CallbackPage() {
  const [searchParams] = useSearchParams();
  const navigate = useNavigate();
  const dispatch = useAppDispatch();
  const notification = useNotification();

  useEffect(() => {
    const state = searchParams.get('state');
    const authorCode = searchParams.get('code') || '';
    const oidcStateString = clientStorage.get(`state.${state}`);
    if (!oidcStateString) {
      //PASSS
      // throw new Error();
      return;
    }
    const oidcState: {
      code_challenge: string;
      code_verifier: string,
      callbackUri: string
    } = JSON.parse(oidcStateString);

    Object.entries(localStorage).map(x => x[0]).filter(x => x.match(/^state./)).forEach(x => clientStorage.remove(x));


    (async () => {
      // Gửi auth code cho BE, nhận lại token & refresh tokne

      try {
        const { data: { accessToken, refreshToken }, code, message } = await authService.sso({
          token: authorCode,
          codeVerifier: oidcState.code_verifier,
          provider: 'AZAD'
        });
        handleHttpStatusCode(code, message);

        //SET HEADER TOKEN TO AXIOS
        http.defaults.headers.common[AUTHORIZATION_KEY] = `${TOKEN_TYPE_KEY} ${accessToken}`;

        //SAVE TOKEN TO LOCAL STORE
        clientStorage.set(ACCESS_TOKEN_KEY, accessToken);
        clientStorage.set(REFRESH_TOKEN_KEY, refreshToken);
        clientStorage.set(USER_NAME, 'VU XUAN HAU');
         await dispatch(fetchUserAsync());

        //  dispatch(setSession({
        //    isLogin: true,
        //    isCheckedLogin: true,
        //    userName: "Vũ Xuân Hậu",
        //  }));

        navigate(oidcState.callbackUri ? oidcState.callbackUri : '/', {
          replace: true
        });
      } catch ({ message }) {
        notification(message as string || 'Lỗi trong quá trình xử lý, vui lòng thử lại!', {
          severity: 'error'
        })
      }

    })();

  }, [dispatch, navigate, searchParams, notification])

  return (
    <LoadingComponent />
  )
}
