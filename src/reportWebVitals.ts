import { ReportHandler } from 'web-vitals';

const reportWebVitals = (onPerfEntry?: ReportHandler) => {
  if (onPerfEntry && onPerfEntry instanceof Function) {
    import('web-vitals').then(({ getCLS, getFID, getFCP, getLCP, getTTFB }) => {
      //CLS (Cumulate Layout Shift)
      getCLS(onPerfEntry);
      //FID (First Input Delay) Thời gian người dùng phản hồi tương tác đầu tiên trên website. Chỉ số FID cần tối ưu để đạt dưới 100 mili giây.
      getFID(onPerfEntry);
      getFCP(onPerfEntry);
      //LCP (Largest Contentful Paint) đo lường Thời gian tải hoàn tất nội dung chính được hiển thị đầu tiên khi trang tải xong. Chỉ số LCP lý tưởng phải đạt 2,5 giây hoặc nhanh hơn.
      getLCP(onPerfEntry);
      getTTFB(onPerfEntry);
    });
  }
};

export default reportWebVitals;
