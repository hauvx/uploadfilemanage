import LoadingComponent from '@components/Loading';
import SetupDevice from '@components/SetupDevice';
import { S18WrapConfirm, S18WrapDialog } from '@core/components/Dialog';
import Progress from '@core/components/Progress';
import Overload from '@core/components/Progress/Overload';
import ProtectedRoute from '@core/components/ProtectedRoute ';
import S18Toast from '@core/components/Toast';
import http from '@core/http';
import { ACCESS_TOKEN_KEY, AUTHORIZATION_KEY, TOKEN_TYPE_KEY } from '@core/utils/appConstant';
import clientStorage from '@core/utils/clientStorage';
import DefaultLayout from '@layouts/DefaultLayout/Components';
import { viVN } from '@mui/material/locale';
import { ThemeProvider, createTheme, useTheme } from '@mui/material/styles';
import { LocalizationProvider } from '@mui/x-date-pickers';
import { AdapterMoment } from '@mui/x-date-pickers/AdapterMoment';
import AuthPage from '@pages/auth';
import CallbackPage from '@pages/auth/Callback';
import ForbiddenPage from '@pages/forbidden';
import NotFoundPage from '@pages/notFound';
import { useAppDispatch, useAppSelector } from '@stores/hook';
import { checkedLogin, fetchUserAsync, setSession } from '@stores/sessionSlice';
import { setAppConfigs } from '@stores/uISlice';
import React, { useEffect, useState } from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import './App.scss';
import IndexPage from './pages';
import FileTemplatePage from '@pages/manage-template/fileTemplate';

function App() {
  const dispatch = useAppDispatch();

  const theme = useTheme();
  const themeWithLocale = createTheme(theme, viVN);
  const [isConneted, setIsConneted] = useState(true);

  useEffect(() => {
    const accessToken = clientStorage.get(ACCESS_TOKEN_KEY);
    if (accessToken) {
      http.defaults.headers.common[AUTHORIZATION_KEY] = `${TOKEN_TYPE_KEY} ${accessToken}`;

      dispatch(fetchUserAsync())
        .finally(() => {
          dispatch(checkedLogin())
        });
      // dispatch(setSession({
      //   isLogin: true,
      //   isCheckedLogin: false,
      //   userName: "Xuân Hậu",
      // }));
      // dispatch(checkedLogin())
    }
    else {
      dispatch(checkedLogin())
    }


    dispatch(setAppConfigs({
      azureADConfig: {
        authority: 'https://login.microsoftonline.com/4ebc9261-871a-44c5-93a5-60eb590917cd/oauth2/v2.0/authorize',
        clientId: '18ed25c3-9150-454f-9950-0281b3d6658f',
        redirectUri: `${window.location.origin}/account/callback`,
        responseType: 'code',
        scope: 'openid email profile',
        responseMode: 'query',
        codeChallengeMethod: 'S256'
      },
      isDevelopmentMode: true
    }))

    dispatch(checkedLogin())
  }, [dispatch])

  const isInited = useAppSelector(state => state.session.isCheckedLogin);
  if (!isInited)
    return <LoadingComponent />;

  if(!isConneted)
    return <SetupDevice />;

  return (
    <ThemeProvider theme={themeWithLocale}>
      <LocalizationProvider dateAdapter={AdapterMoment}>

        <React.Fragment>
          <Progress />
          <BrowserRouter>
            <Routes>
              <Route path='/' element={<DefaultLayout />}>
                <Route element={<ProtectedRoute />}>
                  <Route index element={<IndexPage />} />
                  <Route path='manage-template-excel' element={<FileTemplatePage />} />
                </Route>
              </Route>

              <Route path='auth' element={<AuthPage />} />
              <Route path='login' element={<CallbackPage />} />

              <Route path='403' element={<ForbiddenPage />} />
              <Route path='*' element={<NotFoundPage />} />
            </Routes>
          </BrowserRouter>

          <Overload />
          <S18WrapConfirm />
          <S18WrapDialog />
          <S18Toast />
        </React.Fragment>
      </LocalizationProvider>
    </ThemeProvider>
  );
}
export default App;
