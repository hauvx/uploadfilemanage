import http, { ApiResult } from "@core/http";
import { JwtResultModel } from "./models/JwtResultModel";
import { LoginDevInputDto } from "./models/LoginDevInputDto";
import { SsoLogin } from "./models/SsoLoginDto";

async function sso(body: SsoLogin) {
    const { data } = await http.post<ApiResult<JwtResultModel>>('/auth/sso', body);
    return data;
}

async function otp(emailAddress: string) {
    const { data } = await http.get<ApiResult<string>>(`/auth/otp/${emailAddress}`);
    return data;
}

async function confirmOtp(sessionId: string, otp: string) {
    const { data } = await http.get<ApiResult<JwtResultModel>>(`/auth/otp/Confirm/${sessionId}/${otp}`);
    return data;
}

async function loginDev(body: LoginDevInputDto) {
    const { data } = await http.post<ApiResult<JwtResultModel>>('/auth/login-dev', body);
    return data;
}

async function logout(refreshToken: string | undefined | null) {
    const params = refreshToken ? `?refreshToken=${refreshToken}` : '';
    const { data } = await http.get<ApiResult<JwtResultModel>>(`/auth/logout${params}`);
    return data;
}

const authService = {
    sso,
    loginDev,
    otp,
    confirmOtp,
    logout
}

export default authService;
