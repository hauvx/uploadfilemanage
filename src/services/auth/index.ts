import authService from './auth.service';
import type { SsoLogin } from './models/SsoLoginDto';

export default authService;

export {
    SsoLogin
}

