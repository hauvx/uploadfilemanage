export interface JwtResultModel {
    accessToken: string;
    tokenType: string;
    expiresIn: number;
    refreshToken: string;
}
