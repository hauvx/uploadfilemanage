export interface SsoLogin {
    provider: string;
    token: string;
    codeVerifier: string;
}
