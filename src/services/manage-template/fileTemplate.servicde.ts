import http, { ApiResult } from "@core/http";
import { FileTemplateInput } from "./template";
import { FileTemplateOutput } from "./template/models/templateOutput";


async function GetListFileTemplate(input : FileTemplateInput) {
    const { data } = await http.post<ApiResult<FileTemplateOutput>>('/invt/GetListTemplate',input);
    return data;
}

const templateService = {
    GetListFileTemplate
}

export default templateService;
