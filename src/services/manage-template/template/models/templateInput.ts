import { Sort } from "@core/models/Sort";

export interface FileTemplateInput {
    q?: Query;
    p?: Pagination;
    s?: Sort,
    f?: Filter,
}

export interface Query {
    fileName?: string;
    fromDate?: string;
    toDate?: string;
}
export interface Pagination {
    page?: number;
    rowsPerPage?: number;
}

export interface Filter {
    name: string;
    value: string;
}
