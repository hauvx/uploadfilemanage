export interface FileTemplateOutput {
    id: number,
    concatenatedSegment:string,
    template :string;
    accountingDate: string
}

