import http, { ApiResult } from "@core/http";
import { ListResult } from "@core/models/ListResult";
import { FileTemplateOutput } from "./models/templateOutput";

async function get(body: any) {
    const { data : result } = await http.post<ApiResult<ListResult<FileTemplateOutput>>>('/inv/0407', body);
    return result;
}
async function uploadFile(input : any) {
    const { data } = await http.post<ApiResult<FileTemplateOutput>>('upload',input);
    return data;
}
const fileTemplateService = {
    get,
    uploadFile
}

export default fileTemplateService;
