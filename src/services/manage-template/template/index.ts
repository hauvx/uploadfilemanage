import { FileTemplateInput } from "./models/templateInput";
import fileTemplateService from "./template.service";

export default fileTemplateService;

export type {
    FileTemplateInput
}
