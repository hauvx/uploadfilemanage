import http, { ApiResult } from "@core/http";
import type { MeDtoOutput } from "./models/MeDtoOutput";

async function me() {
    const { data } = await http.get<ApiResult<MeDtoOutput>>('/me');
    return data
}

async function existByEmailOrUsername(keyword: string) {
    const { data } = await http.get<ApiResult<boolean>>(`/exist/${keyword}`);
    return data
}

const userService = {
    me,
    existByEmailOrUsername
}

export default userService;
