import { BaseEntity } from "@core/models/BaseEntity";

export interface User extends BaseEntity<number> {
    userName: string;
}
