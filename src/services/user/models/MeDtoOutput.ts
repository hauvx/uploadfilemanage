import { User } from "./User";

export interface MeDtoOutput {
    userInfo: User;
    roles: string[];
    permissions: string[];
}
