import { S18IconProps } from "."
import S18GoogleFontIcon from "./GoogleFontIcon"
import S18SvgIcon from "./SvgIcon"

export default function S18Icon({ provider, ...props }: S18IconProps) {

  if (provider === 'svg')
    return (
      <S18SvgIcon {...props} />
    )

  if (provider === 'font-awesome-icon')
    return (
      <i className="fa-regular fa-bell-exclamation"></i>
    )

  return (
    <S18GoogleFontIcon {...props} />
  )
}
