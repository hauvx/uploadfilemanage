import { Icon } from '@mui/material';
import { CommonProps } from '@mui/material/OverridableComponent';
export declare type GoogleFontIconType = 'outlined' | 'round' | 'sharp' | 'two-tone';

declare type GoogleFontIconProps = CommonProps & {
  children?: string,
  type?: GoogleFontIconType
}
export default function S18GoogleFontIcon({ children, type, ...props }: GoogleFontIconProps) {
  if (type)
    return (
      <Icon {...props}
        className={`material-icons-${type} + ${props.className}`}
      >
        {children}
      </Icon>
    )

  return (
    <Icon {...props}>{children}</Icon>
  )
}
