import { CommonProps } from "@mui/material/OverridableComponent";

export type FontIconType = 'outlined' | 'round' | 'sharp' | 'two-tone';

export interface S18IconOptions {
    provider?: 'google-icon' | 'svg' | 'font-awesome-icon',
    children?: string,
    type?: FontIconType
  }

  export interface S18IconProps extends S18IconOptions, CommonProps {}
