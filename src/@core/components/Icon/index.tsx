import S18Icon from './S18Icon';
import type { FontIconType, S18IconOptions, S18IconProps } from './type';

export default S18Icon;

export {
  S18IconOptions,
  S18IconProps,
  FontIconType
};

