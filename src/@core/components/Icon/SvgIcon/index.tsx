import { SvgIcon } from '@mui/material';
import { CommonProps } from '@mui/material/OverridableComponent';


export declare type S18SvgIconProps = {
  children?: string
} & CommonProps;

export default function S18SvgIcon({ children, ...props }: S18SvgIconProps) {
  return (
    <SvgIcon {...props}>
      <path d={children} />
    </SvgIcon>
  )
}
