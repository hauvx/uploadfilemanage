import { DialogTitle, IconButton } from "@mui/material";
import S18Icon, { S18IconOptions } from "../Icon";

export interface S18DialogTitleProps {
  children?: React.ReactNode,
  isWaiting?: boolean,
  isHideCloseIcon?: boolean,
  closeIcon?: S18IconOptions
  handleCancel: any,
  isHidden?: boolean
}

export default function S18DialogTitle(
  {
    children,
    isHideCloseIcon,
    closeIcon,
    isHidden,
    handleCancel,
    isWaiting
  }: S18DialogTitleProps
) {

  if (!closeIcon) {
    closeIcon = {
      children: 'close',
      type: 'outlined'
    }
  }

  if (isHidden)
    return null;

  return (
    <DialogTitle sx={{
      borderBottom: '1px solid #efefef'
    }}>
      {children}
      {!isHideCloseIcon && <IconButton
        aria-label="close"
        disabled={isWaiting}
        onClick={handleCancel}
        sx={{
          position: 'absolute',
          right: 8,
          top: 8,
        }}
      >
        <S18Icon {...closeIcon}>{closeIcon?.children}</S18Icon>
      </IconButton>}
    </DialogTitle>
  )
}
