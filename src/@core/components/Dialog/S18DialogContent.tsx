import { DialogContent } from '@mui/material'

export default function S18DialogContent({ children }: any) {
  return (
    <DialogContent className='mt-2'>
      {children}
    </DialogContent>
  )
}
