import { Dialog } from '@mui/material';
import { S18DialogProps } from './type';

export default function S18Dialog({ isOpen, handleClose, children, isWaiting, size }: S18DialogProps) {
  const _handleClose = (event: object, reason: string) => {
    if (isWaiting)
      return;
    if (reason === 'backdropClick')
      return;
    handleClose();
  }
  return (
    <Dialog fullWidth maxWidth={size} open={isOpen} onClose={_handleClose}>
      {children}
    </Dialog>
  )
}
