import S18Dialog from './S18Dialog';
import S18DialogAction from './S18DialogAction';
import S18DialogTitle from './S18DialogTitle';
import S18WrapConfirm from './S18WrapConfirm';
import S18WrapDialog from './S18WrapDialog';
import type { S18DialogActionButtonOptions } from './type';

export {
  S18DialogAction,
  S18DialogTitle,
  S18DialogActionButtonOptions,
  S18WrapDialog,
  S18WrapConfirm
};

export default S18Dialog;
