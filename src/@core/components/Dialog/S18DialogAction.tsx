import { Button, CircularProgress, DialogActions } from '@mui/material';
import { S18DialogActionButtonOptions } from '.';
import S18Icon, { S18IconOptions } from '../Icon';


function ActionIconDialogButton({ isWaiting, icon }: {
  isWaiting?: boolean, icon?: {
    isShow: boolean,
    info: S18IconOptions
  }
}) {
  if (!icon?.isShow)
    return null;
  if (isWaiting)
    return <CircularProgress size={20} color="primary" />;

  return <S18Icon {...icon.info}>{icon.info.children}</S18Icon>
}

export default function S18DialogAction({ isWaitingOk, isWaitingCancle, okOptions, cancleOptions }: {
  isWaitingOk?: boolean,
  isWaitingCancle?: boolean,
  okOptions: S18DialogActionButtonOptions,
  cancleOptions?: S18DialogActionButtonOptions,
  handleOk?: () => void,
  handleCancel?: () => void,
  actionOptions?: any
}) {
  return (
    <DialogActions className='flex p-3 pr-6' sx={{
      borderTop: '1px solid #efefef'
    }}>
      <Button
        className='order-2'
        disabled={isWaitingOk || isWaitingCancle}
        variant='contained'
        startIcon={<ActionIconDialogButton icon={okOptions.icon} isWaiting={isWaitingOk} />}
        onClick={okOptions.handle}
      >
        {okOptions?.text || 'Đồng ý'}
      </Button>
      {
        (!cancleOptions || cancleOptions?.isHidden) ? null : <Button
          className='order-1'
          disabled={isWaitingOk || isWaitingCancle}
          variant='contained'
          color='error'
          startIcon={<ActionIconDialogButton icon={cancleOptions?.icon} isWaiting={isWaitingCancle} />}
          onClick={cancleOptions?.handle}
        >
          {cancleOptions?.text || 'Hủy'}
        </Button>
      }

    </DialogActions>
  )
}
