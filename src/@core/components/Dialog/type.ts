import { Breakpoint } from "@mui/material"
import { S18IconOptions } from "../Icon"

export interface DialogTitleOptions {
    /**
     * Tiêu đề của dialog
     * @default undefined
     */
    title?: string,
    /**
     * Ẩn đi title (ẩn tất cả thông tin liên quan đến title)
     * @default false
     */
    isHidden?: boolean,
    /**
     * Hiển thị icon đóng dialog trên title
     * @default true
     */
    isShowCloseIcon?: boolean,
    /**
     * Icon đóng dialog, danh sách icon @see [Google Icon Font](https://fonts.google.com/icons?icon.set=Material+Icons)
     * @default close
     */
    closeIcon?: S18IconOptions
}


export interface S18DialogProps  {
    /**
     * Kích thước tối đa của dialog
     * @default md
     */
     size?: Breakpoint,

     /**
     * Cách xử lý khi nội dung bị tràn:
     * - paper: dialog sẽ được giới hạn dựa trên vw hiện tại, nội dung sẽ được scroll trong phạm vi của dialog content
     * - body: Dialog sẽ scroll trên body
     * @default paper
     */
    scroll?: 'paper' | 'body',

    /**
     * Trạng thái đóng/mở dialog hiện tại
     * @default false: Dialog đang đóng
     */
    isOpen: boolean,

    /**
     * Hàm điều khiển trạng thái đóng mở
     */
    handleClose: any,
    children: any,
    isWaiting?: boolean
}


/**
 * Các tùy chọn cho dialog
 */
export interface DialogOptions {
    /**
     * Các tùy chọn cấu hình dialog title
     * @default {
     *    isHidden: false,
     *    isShowCloseIcon: true,
     *    closeIcon: CloseOutlinedIcon
     * }
     */
    titleOptios?: DialogTitleOptions
    /**
     * Nội dung bên trong dialog
     * @default undefined
     */
    children?: string | React.ReactNode,
    /**
     * Kích thước tối đa của dialog
     * @default md
     */
    size?: Breakpoint,
    /**
     * Cách xử lý khi nội dung bị tràn:
     * - paper: dialog sẽ được giới hạn dựa trên vw hiện tại, nội dung sẽ được scroll trong phạm vi của dialog content
     * - body: Dialog sẽ scroll trên body
     * @default paper
     */
    scroll?: 'paper' | 'body',
    /**
     * Hàm xử lý các tương tác của người dùng
     */
    handle?: {
        /**
         * Xử lý khi người dùng nhấn button cancel hoặc button esc
         * - Hàm này được xử lý trước khi dialog được đóng
         * - Nếu kết quả trả về là false dialog sẽ không đóng, ngược lại sẽ đóng dialog
         */
        handleCancel?: () => Promise<boolean>,
        /**
         * Xử lý khi người dùng nhấn button ok
         * - Hàm này được xử lý trước khi dialog được đóng
         * - Nếu kết quả trả về là false dialog sẽ không đóng, ngược lại sẽ đóng dialog
         */
        handleOk?: () => Promise<boolean>,
    },
    actionOptions?: {
        /**
             * Text hiển thị ở button Ok
             * @default 'Lưu'
             */
        okText?: string,
        /**
         * Icon đóng dialog, danh sách icon @see [Google Icon Font](https://fonts.google.com/icons?icon.set=Material+Icons)
         * @default done
         */
        okIcon?: string,
        /**
         * Text hiển thị ở button Ok
         */
        cancelText?: string,
        /**
         * Icon đóng dialog, danh sách icon @see [Google Icon Font](https://fonts.google.com/icons?icon.set=Material+Icons)
         * @default done
         */
        cancelIcon?: string
    }
}


export interface S18DialogActionButtonOptions {
    isHidden?: boolean,
    text?: string,
    handle?: any,

    icon?: {
      isShow: boolean,
      info: S18IconOptions
    }
  }
