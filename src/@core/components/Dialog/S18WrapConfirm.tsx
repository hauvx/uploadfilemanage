import { useAppSelector } from '@stores/hook';
import React from 'react'

export default function S18WrapDialog() {
  const confirmElements = useAppSelector(state => state.ui.confirmInfos.map(x => x.element));
  return (
    <React.Fragment>{ confirmElements }</React.Fragment>
  )
}
