import { useAppSelector } from '@stores/hook';
import React from 'react'

export default function S18WrapDialog() {
  const dialogElements = useAppSelector(state => state.ui.dialogInfos.map(x => x.element));
  return (
    <React.Fragment>{ dialogElements }</React.Fragment>
  )
}
