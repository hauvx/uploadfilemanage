import { Box } from "@mui/material";

declare type S18TabPanelProps = {
  children: React.ReactElement,
  index: number,
  value: number
}

export default function S18TabPanel(props: S18TabPanelProps) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`vertical-tabpanel-${index}`}
      aria-labelledby={`vertical-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          {children}
        </Box>
      )}
    </div>
  );
}
