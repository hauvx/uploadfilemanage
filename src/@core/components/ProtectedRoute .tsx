import { useAppSelector } from '@stores/hook';
import qs from 'qs';
import { useEffect, useState } from 'react';
import { Navigate, Outlet, useNavigate } from 'react-router-dom';
declare type ProtectedRouteProps = {
  role?: string | string[],
  permission?: string | string[],
  children?: JSX.Element,
};


export default function ProtectedRoute({ role, permission, children }: ProtectedRouteProps) {
  const navigate = useNavigate();

  const isLogin = useAppSelector(state => state.session.isLogin);
  const [navigateTo, setNavigateTo] = useState('/auth');
  const callbackUri = window.location.pathname;
  useEffect(() => {
    const objNavigate = {} as { [key: string]: any };
    if (callbackUri && callbackUri !== '/' && !callbackUri.match(/^(\/)?auth(\/)?/))
      objNavigate.callbackUri = callbackUri;


    const queryParamsString = qs.stringify(objNavigate);

    setNavigateTo(`/auth?${queryParamsString}`);
    if (!isLogin){
      navigate(`/auth?${queryParamsString}`)
    }
  }, [callbackUri, isLogin, navigate])

  if (!isLogin){
    return (
      <Navigate to={navigateTo} replace={true} />
    );
  }


  return children ? children : <Outlet />;
}
