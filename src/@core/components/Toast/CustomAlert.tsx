import { Alert, AlertColor } from '@mui/material';
import { useAppDispatch } from '@stores/hook';
import { removeNotification } from '@stores/uISlice';
import { useEffect } from 'react';
import './CustomAlert.scss';

export default function CustomAlert({ children, refId, severity }: {
  children?: any,
  refId: string | number,
  severity?: AlertColor
}) {

  const dispatch = useAppDispatch();
  const handleClose = () => {
    dispatch(removeNotification(refId));
  }

  useEffect(() => {
    const timeoutId = setTimeout(() => {
      handleClose();
    }, 5000);
    return () => {
      clearTimeout(timeoutId)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])


  return (
    <Alert severity={severity || 'success'} sx={{ minWidth: 320 }} onClose={handleClose} color={severity || 'success'} >
      {children}
    </Alert>
  )
}
