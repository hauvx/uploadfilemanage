import { Stack } from '@mui/material'
import { useAppSelector } from '@stores/hook'
import CustomAlert from './CustomAlert'

export default function S18Toast() {

  const notifications = useAppSelector(state => state.ui.notifications)

  return (
    <Stack className=' right-3 bottom-3 fixed z-[2000]' spacing={1}>
      {
        notifications.map(noti => <CustomAlert key={noti.refId} {...noti}>{noti.children}</CustomAlert>)
      }
    </Stack>
  )
}
