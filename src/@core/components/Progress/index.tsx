import { Backdrop, CircularProgress } from '@mui/material';
import { useAppSelector } from '@stores/hook';

export default function Progress() {

  const isShowLoading = useAppSelector(state => state.ui.loadingKeys.some(x => true));

  if(!isShowLoading)
    return null;

  return (
    <Backdrop open={true} className='z-[100000]'>
      <CircularProgress />
    </Backdrop>
  )
}
