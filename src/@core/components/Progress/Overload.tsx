import { useAppSelector } from '@stores/hook';

export default function Overload() {
  const isShowOverload = useAppSelector(state => state.ui.overloadKeys.some(x => true));
  return (
    <div id='bg-overlay' className={`bg-overlay ${isShowOverload ? 'active' : ''}`}></div>
  )
}
