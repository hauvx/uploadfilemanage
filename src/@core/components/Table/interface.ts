
/**
 * Các tùy chọn cho cột trong table
 */
export interface S18TableColumnOptions {
    /**
     * Có được sort trên cột này không
     * @default true: Được phép sort
     */
    allowSort?: boolean,

    /**
     * Được phép ẩn đi cột này
     * @default true: Được phép ẩn đi cột này
     */
    alayShow?: boolean

    /**
     * Hiển thị cột này
     * @default true: Mặc định sẽ hiển thị cột này
     */
    isShow?: boolean
}

/**Định nghĩa một cột trong table */
export interface S18TableColumnDef {
    /**Tên cột (là khóa chính của cột) */
    name: string,
    /**Tên hiển thị của cột */
    label: string,
    options: S18TableColumnOptions & { [key: string]: any }
}

/**Định nghĩa các thuộc tính của một table */
export interface S18TableProps {
    /**Tên hiển thị của table */
    title?: string,
    /**Danh sách định nghĩa các cột có trong table */
    columns?: S18TableColumnDef[],
    /**Dữ liệu render trong table */
    data?: { [key: string]: any }[],
    /**Các tùy chọn khác */
    options?: S18TableToobarOptions & { [key: string]: any }
}

export interface S18TableToobarOptions {

}
