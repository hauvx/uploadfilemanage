import S18Icon from '@core/components/Icon';
import { FormControlLabel, IconButton, Switch, Tooltip } from '@mui/material';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import React from 'react';
import { S18TableColumnDef } from '../interface';

export default function S18TableViewColumn({ columns }: {
  columns?: S18TableColumnDef[]
}) {
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);


  const open = Boolean(anchorEl);
  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <React.Fragment>
      <Tooltip title='Tùy chọn cột hiển thị'>
        <IconButton onClick={handleClick}>
          <S18Icon type='outlined'>
            view_column
          </S18Icon>
        </IconButton>
      </Tooltip>
      <Menu
        id="basic-menu"
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        MenuListProps={{
          'aria-labelledby': 'basic-button',
        }}
      >
        {
          columns?.map(col => <MenuItem key={col.name}>
            <FormControlLabel
              control={<Switch disabled={col.options.alayShow} defaultChecked color="primary" />}
              label={col.label}
            />
          </MenuItem>)
        }
      </Menu>
    </React.Fragment>
  )
}
