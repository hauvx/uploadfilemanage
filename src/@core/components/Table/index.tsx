import { IconButton, TablePagination, Tooltip } from '@mui/material';
import React, { useMemo } from 'react'
import S18Icon from '../Icon';
import './index.scss';
import type { S18TableProps, S18TableColumnDef, S18TableColumnOptions } from './interface';
import S18TableViewColumn from './TableHeader/ViewColumn';



export default function S18Table({ title, columns, data }: S18TableProps) {

  const gridTemplateColumns = useMemo(() => {
    let gridTemplateColumns = '';
    for (const column of columns || []) {
      const columnWidth = column.options.width;
      if (!columnWidth) {
        gridTemplateColumns += 'auto '
      }
      if (typeof columnWidth === typeof Number()) {
        gridTemplateColumns += `${columnWidth}px `
      }
      if (typeof columnWidth === typeof String()) {
        gridTemplateColumns += `${columnWidth} `
      }
    }
    return gridTemplateColumns;
  }, [columns])


  return (
    <div className="S18Table">
      <div className="S18TableHeader">
        <div className="S18TableHeader-left">
          <h6 className='S18TableHeader-left-title'>{title}</h6>
        </div>
        <div className="S18TableHeader-right">
          <div className='S18TableHeader-left-toolbar'>
            <Tooltip title='Tải xuống file excel'>
              <IconButton>
                <S18Icon type='outlined'>
                  cloud_download
                </S18Icon>
              </IconButton>
            </Tooltip>
            <S18TableViewColumn columns={columns} />

          </div>
        </div>
      </div>

      <div className="S18TableContent scrollbar">
        <div className="S18TableContent-header" style={{
          gridTemplateColumns
        }}>
          {
            columns?.map(col => <div className='cell' key={col.name}>
              {col.label}
            </div>)
          }
        </div>

        <div className="S18TableContent-body" style={{
          gridTemplateColumns
        }}>
          {
            data?.map(row =>
              <div key={row.id} className="S18TableContent-body-row" style={{
                gridTemplateColumns
              }}>
                {
                  columns?.map(col => <div className='cell' key={col.name}>
                    {row[col.name]}
                  </div>)
                }
              </div>)
          }
        </div>
      </div>

      <div className="S18TableFooter">
        <TablePagination
          component="div"
          count={100}
          page={1}
          onPageChange={() => { }}
          rowsPerPage={20}
          onRowsPerPageChange={() => { }}
        />
      </div>

    </div >
  )
}

export {
  S18TableProps,
  S18TableColumnDef,
  S18TableColumnOptions
}
