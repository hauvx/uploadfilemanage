const filterNonNull = (obj: any) => {
    return Object.fromEntries(Object.entries(obj).filter(([k, v]) => v));
}

const  objectHelper = {
    filterNonNull
}

export default objectHelper;
