function convertToArray(_input?: string | string[]): string[] {
  if (!_input)
    return []
  return Array.isArray(_input) ? _input : [_input];
}

const convert = {
  convertToArray
};

export default convert;
