export const ACCESS_TOKEN_KEY = 'access_token';
export const TOKEN_TYPE_KEY = 'Bearer';
export const REFRESH_TOKEN_KEY = 'refresh_token';
export const AUTHORIZATION_KEY = 'Authorization';
export const USER_NAME = 'User_name';
export const scheduleType = [
  {value:"syncProject",label:"Đồng bộ filter project"},
  {value:"companyCode",label:"Đồng bộ filter company"},
  {value:"orgCode",label:"Đồng bộ filter Org"}
]
