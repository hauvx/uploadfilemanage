export default function getAvatarName(familyName?: string, givenName?: string) {
    if (!familyName || !givenName)
        return null;
    return `${familyName[0]}${givenName?.split(' ')?.pop()?.split('')[0]}`
}
