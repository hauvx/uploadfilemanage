export function isExistInArray(target: string[] = [], destination: string[] = []) {
    if (target.length === 0) return true;
    return target.some(el => destination.includes(el));
}