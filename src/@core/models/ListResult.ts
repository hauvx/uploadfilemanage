export interface ListResult<T> {
    totalCount: number;
    items: T[];
    query?: any;
}
