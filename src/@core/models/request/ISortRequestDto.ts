export interface ISortRequestDto {
    orderBy: string;
    isSortDesc: boolean;
}
