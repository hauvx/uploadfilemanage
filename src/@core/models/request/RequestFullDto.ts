export interface RequestFullDto {
    rowsPerPage: number;
    page: number;
    orderBy: string;
    isSortDesc: boolean;
}
