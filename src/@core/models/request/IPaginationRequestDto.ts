export interface PaginationRequestDto {
    rowsPerPage: number;
    page: number;
}
