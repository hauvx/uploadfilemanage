
export interface BaseEntity<TPrimary> {
  id: TPrimary;
}
