import { Breakpoint, Button, CircularProgress, Dialog, DialogActions, DialogContent, DialogTitle, Icon, IconButton } from '@mui/material';
import { useAppDispatch } from '@stores/hook';
import { pushDialog, removeDialog } from '@stores/uISlice';
import moment from 'moment';
import React, { useState } from 'react';

/**
 * Các tùy chọn cho dialog
 */
export declare type DialogOptions = {
  /**
   * Các tùy chọn cấu hình dialog title
   * @default {
   *    isHidden: false,
   *    isShowCloseIcon: true,
   *    closeIcon: CloseOutlinedIcon
   * }
   */
  titleOptios?: DialogTitleOptions
  /**
   * Nội dung bên trong dialog
   * @default undefined
   */
  content?: string | React.ReactNode,
  /**
   * Kích thước tối đa của dialog
   * @default md
   */
  size?: Breakpoint,
  /**
   * Cách xử lý khi nội dung bị tràn:
   * - paper: dialog sẽ được giới hạn dựa trên vw hiện tại, nội dung sẽ được scroll trong phạm vi của dialog content
   * - body: Dialog sẽ scroll trên body
   * @default paper
   */
  scroll?: 'paper' | 'body',
  /**
   * Hàm xử lý các tương tác của người dùng
   */
  handle?: {
    /**
     * Xử lý khi người dùng nhấn button cancel hoặc button esc
     * - Hàm này được xử lý trước khi dialog được đóng
     * - Nếu kết quả trả về là false dialog sẽ không đóng, ngược lại sẽ đóng dialog
     */
    handleCancel?: () => Promise<boolean>,
    /**
     * Xử lý khi người dùng nhấn button ok
     * - Hàm này được xử lý trước khi dialog được đóng
     * - Nếu kết quả trả về là false dialog sẽ không đóng, ngược lại sẽ đóng dialog
     */
    handleOk?: () => Promise<boolean>,
  },
  actionOptions?: {
    /**
         * Text hiển thị ở button Ok
         * @default 'Lưu'
         */
    okText?: string,
    /**
     * Icon đóng dialog, danh sách icon @see [Google Icon Font](https://fonts.google.com/icons?icon.set=Material+Icons)
     * @default done
     */
    okIcon?: string,
    /**
     * Text hiển thị ở button Ok
     */
    cancelText?: string,
    /**
     * Icon đóng dialog, danh sách icon @see [Google Icon Font](https://fonts.google.com/icons?icon.set=Material+Icons)
     * @default done
     */
    cancelIcon?: string
  }
}

/**
 * Các tùy chọn dành cho title dialog
 */
declare type DialogTitleOptions = {
  /**
   * Tiêu đề của dialog
   * @default undefined
   */
  title?: string,
  /**
   * Ẩn đi title (ẩn tất cả thông tin liên quan đến title)
   * @default false
   */
  isHidden?: boolean,
  /**
   * Hiển thị icon đóng dialog trên title
   * @default true
   */
  isShowCloseIcon?: boolean,
  /**
   * Icon đóng dialog, danh sách icon @see [Google Icon Font](https://fonts.google.com/icons?icon.set=Material+Icons)
   * @default close
   */
  closeIcon?: string
}

const defaultDialogOptions: DialogOptions = {
  titleOptios: {
    isHidden: false,
    isShowCloseIcon: true,
    closeIcon: 'close'
  },
  content: 'Dữ liệu sẽ không thể khôi phục sau khi đồng ý, bạn có muốn tiếp tục?',
  size: 'md',
  scroll: 'paper',
  actionOptions: {
    okText: 'Xong',
    okIcon: 'done',
    cancelText: 'Hủy',
    cancelIcon: 'clear'
  }
}

/**
 * Props của DialogComponent
 */
declare type DialogComponentProps = DialogOptions & {
  /**
   * Khóa chính của dialog, dùng để tìm, xóa nó trong tương lai
   */
  refId: string | number
}

function DialogTitleComponent(
  {
    children,
    isShowCloseIcon,
    closeIcon,
    isHidden,
    handleCancel,
    isWaiting
  }: { children?: React.ReactNode, isWaiting?: boolean, handleCancel: () => Promise<void> } & DialogTitleOptions
) {
  if (isHidden)
    return null;
  return (
    <DialogTitle sx={{
      borderBottom: '1px solid #efefef'
    }}>
      {children}
      {isShowCloseIcon && <IconButton
        aria-label="close"
        disabled={isWaiting}
        onClick={handleCancel}
        sx={{
          position: 'absolute',
          right: 8,
          top: 8,
        }}
      >
        <Icon>{closeIcon}</Icon>
      </IconButton>}
    </DialogTitle>
  )
}


function ActionIconDialogComponent({ isWaiting, iconText }: { isWaiting: boolean, iconText?: string }) {
  if (!iconText)
    return null;
  if (isWaiting)
    return <CircularProgress size={20} color="primary" />;
  return <Icon>{iconText}</Icon>
}

function DialogComponent({ refId, ...props }: DialogComponentProps) {
  const [isOpen, setIsOpen] = useState(true);
  const [isWaitingOk, setIsWaitingOk] = useState(false);
  const [isWaitingCancle, setIsWaitingCancle] = useState(false)
  const dispatch = useAppDispatch();
  const handleClose = (event: object, reason: string) => {
    if (isWaitingOk || isWaitingCancle)
      return;
    if (reason === 'backdropClick')
      return;
    handleCancel();
  }

  const handleCancel = async () => {
    let allowClose = true;
    if (props.handle?.handleCancel) {
      setIsWaitingCancle(true);
      allowClose = await props.handle?.handleCancel();
    }
    if (!allowClose) {
      setIsWaitingCancle(false);
      return;
    }
    setIsOpen(false);
    setIsWaitingCancle(false);
    dispatch(removeDialog(refId));

  }

  const handleOk = async () => {
    let allowClose = true;
    if (props.handle?.handleOk) {
      setIsWaitingOk(true);
      allowClose = await props.handle?.handleOk();
    }
    if (!allowClose) {
      setIsWaitingOk(false);
      return;
    }
    setIsOpen(false);
    setIsWaitingOk(false);
    dispatch(removeDialog(refId));
  }

  return (
    <Dialog
      open={isOpen}
      onClose={handleClose}
      maxWidth={props.size}
      fullWidth={true}
      scroll={props.scroll}
      disableEscapeKeyDown={false}
    >
      <DialogTitleComponent
        isWaiting={isWaitingOk || isWaitingCancle}
        handleCancel={handleCancel} {...props.titleOptios}
      >
        {props.titleOptios?.title}
      </DialogTitleComponent>
      <DialogContent>
        {props.content}
      </DialogContent>
      <DialogActions className='flex' sx={{
        borderTop: '1px solid #efefef'
      }}>
        <Button
          className='order-2'
          disabled={isWaitingOk || isWaitingCancle}
          startIcon={<ActionIconDialogComponent iconText={props.actionOptions?.okIcon} isWaiting={isWaitingOk} />}
          onClick={handleOk}
        >
          {props.actionOptions?.okText}
        </Button>
        <Button
          className='order-1'
          disabled={isWaitingOk || isWaitingCancle}
          color='error'
          startIcon={props.actionOptions?.cancelIcon ? <Icon>{props.actionOptions?.cancelIcon}</Icon> : undefined}
          onClick={handleCancel}
        >
          {props.actionOptions?.cancelText}
        </Button>
      </DialogActions>
    </Dialog>
  )
}

/**
 * Hook hỗ trợ dialog
 * @returns Đối tượng control dialog
 */
export default function useDialog() {
  const dispatch = useAppDispatch();

  return {
    open: (options?: DialogOptions) => {
      const refId = moment().format('DDMMYYYYHHmmssSS');
      const dialogProps: DialogComponentProps = {
        titleOptios: {
          title: options?.titleOptios?.title ?? defaultDialogOptions.titleOptios?.title,
          isHidden: options?.titleOptios?.isHidden ?? defaultDialogOptions.titleOptios?.isHidden,
          isShowCloseIcon: options?.titleOptios?.isShowCloseIcon ?? defaultDialogOptions.titleOptios?.isShowCloseIcon,
          closeIcon: options?.titleOptios?.closeIcon ?? defaultDialogOptions?.titleOptios?.closeIcon
        },
        content: options?.content ?? defaultDialogOptions.content,
        scroll: options?.scroll ?? defaultDialogOptions.scroll,
        size: options?.size ?? defaultDialogOptions.size,
        handle: options?.handle,
        actionOptions: {
          okText: options?.actionOptions?.okText ?? defaultDialogOptions?.actionOptions?.okText,
          okIcon: options?.actionOptions?.okIcon ?? defaultDialogOptions?.actionOptions?.okIcon,
          cancelText: options?.actionOptions?.cancelText ?? defaultDialogOptions?.actionOptions?.cancelText,
          cancelIcon: options?.actionOptions?.cancelIcon ?? defaultDialogOptions?.actionOptions?.cancelIcon,
        },
        refId
      }
      dispatch(pushDialog({
        refId,
        element: <DialogComponent key={refId} {...dialogProps} />
      }));
      return {
        refId,
        destroy: () => {
          dispatch(removeDialog(refId));
        }
      }
    }
  }
}
