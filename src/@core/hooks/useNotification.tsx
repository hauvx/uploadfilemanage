import { AlertColor } from '@mui/material';
import { useAppDispatch } from '@stores/hook';
import { queueNotification } from '@stores/uISlice';
import moment from 'moment';

/**
 * Hook hỗ trợ noti
 * @returns Noti function
 */
export default function useNotification() {
  const dispatch = useAppDispatch()
  return (message: string, options?: {
    severity: AlertColor
  }) => {
    const refId = 'noti-ref-id-' + moment().format('YYMDHSS') + '-' + Math.floor(Math.random() * (1000000 - 1))
    dispatch(queueNotification({
      refId,
      children: message,
      severity: options?.severity
    }))
  }
}

