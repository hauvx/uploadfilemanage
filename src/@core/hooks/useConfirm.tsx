import { Button, Dialog, DialogActions, DialogContent, DialogTitle } from '@mui/material';
import { useAppDispatch } from '@stores/hook';
import { pushConfirm, removeConfirm } from '@stores/uISlice';
import moment from 'moment';
import React, { useEffect, useRef, useState } from 'react';
import DoneIcon from '@mui/icons-material/Done';
import ClearIcon from '@mui/icons-material/Clear';
/**
 * Các tùy chọn khi show confirm
 */
export declare type ConfirmOptions = {
    /**
     * Text hiển thị trong button ok
     * @default Đồng ý
     */
    okText?: string,
    /**
     * Text hiển thị trong botton Cancel
     * @default Hủy
     */
    cancelText?: string,
    /**
     * Title của dialog confirm
     * @default Xác nhận yêu cầu!
     */
    title?: string,
    /**
     * Nội dung của dialog confirm
     * @default Dữ liệu sẽ không thể khôi phục sau khi đồng ý, bạn có muốn tiếp tục?
     */
    content?: string | React.ReactNode
}

const defaultConfirmOptions: ConfirmOptions = {
    okText: 'Đồng ý',
    cancelText: 'Hủy',
    title: 'Xác nhận yêu cầu!',
    content: 'Dữ liệu sẽ không thể khôi phục sau khi đồng ý, bạn có muốn tiếp tục?'
}

declare type CofirmComponentProps = ConfirmOptions & {
    resolveFC?: (value: boolean | PromiseLike<boolean>) => void,
    refId: string | number
}


function ConfirmComponent({ resolveFC, refId, ...props }: CofirmComponentProps) {
    const [isOpen, setIsOpen] = useState(true);
    const dispatch = useAppDispatch();
    const dialogActionElementRef = useRef<HTMLButtonElement>(null);

    useEffect(() => {

        if (isOpen) {
            setTimeout(() => {
                const { current: dialogActionElement } = dialogActionElementRef;
                if (dialogActionElement !== null) {
                    dialogActionElement.focus();
                }
            }, 0);

        }
    }, [isOpen])

    const handleClose = () => {
        setIsOpen(false);
        dispatch(removeConfirm(refId),);
        setTimeout(() => {
            resolveFC && resolveFC(false);
        }, 0);

    }

    const handleOk = () => {
        setIsOpen(false);
        dispatch(removeConfirm(refId));
        setTimeout(() => {
            resolveFC && resolveFC(true);
        }, 0);
    }

    return (
        <Dialog open={isOpen} onClose={handleClose} maxWidth={'sm'} fullWidth={true}>
            <DialogTitle>{props.title}</DialogTitle>
            <DialogContent>
                {props.content}
            </DialogContent>
            <DialogActions className='flex' ref={dialogActionElementRef}>
                <Button  variant="contained" className='order-2 text-[#fff] ml-[10px]' onClick={handleOk} startIcon={<DoneIcon />}>{props.okText}</Button>
                <Button className='order-1 bg-[red] text-[#fff] border-[red] hover:bg-[red] hover:border-[red]' variant="outlined" startIcon={<ClearIcon className='text-[#fff] ' />} onClick={handleClose}>{props.cancelText}</Button>
            </DialogActions>
        </Dialog>
    )
}

/**
 * Hook hỗ trợ confirm
 * @returns Đối tượng điều khiển confirm
 */
export default function useConfirm() {
    const dispatch = useAppDispatch();
    return {
        show: (options?: ConfirmOptions) => new Promise<boolean>((resolve) => {
            const confirmProps: ConfirmOptions = {
                okText: options?.okText ?? defaultConfirmOptions.okText,
                cancelText: options?.cancelText ?? defaultConfirmOptions.cancelText,
                title: options?.title ?? defaultConfirmOptions.title,
                content: options?.content ?? defaultConfirmOptions.content,
            }
            const refId = moment().format('DDMMYYYYHHmmssSS');
            const element = React.createElement(
                ConfirmComponent,
                { resolveFC: resolve, key: refId, refId, ...confirmProps },
                null
            );
            dispatch(pushConfirm({
                refId,
                element
            }));
        })
    }
}
