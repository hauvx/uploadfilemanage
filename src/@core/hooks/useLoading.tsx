import { useAppDispatch } from "@stores/hook";
import { pushLoading, resolveLoading } from "@stores/uISlice";
import moment from "moment";

export default function useLoading() {
  const dispatch = useAppDispatch();
  const newKey = () => 'noti-ref-id-' + moment().format('YYMDHSS') + '-' + Math.floor(Math.random() * (1000000 - 1))
  const show = (key: string) => {
    dispatch(pushLoading(key))
  }
  const resolve = (key: string) => {
    dispatch(resolveLoading(key))
  }
  return {
    newKey,
    show,
    resolve
  }
}
