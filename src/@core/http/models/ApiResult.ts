import { HttpStatusCode } from "./HttpStatusCode";

export interface BaseResult {
  status: string;
  code: HttpStatusCode;
  message: string;
  systemName: string;
}

export interface ApiResult<TData> extends BaseResult {
  data: TData;
}
