import { ACCESS_TOKEN_KEY, AUTHORIZATION_KEY, REFRESH_TOKEN_KEY, TOKEN_TYPE_KEY } from "@core/utils/appConstant";
import clientStorage from "@core/utils/clientStorage";
import { JwtResultModel } from "@services/auth/models/JwtResultModel";
import axios, { AxiosRequestConfig, AxiosResponse } from "axios";
import qs from 'qs';
import type { ApiResult } from './models/ApiResult';

const http = axios.create({
    baseURL: process.env.REACT_APP_REMOTE_SERVICE_BASE_URL,
    timeout: 3 * 60 * 1000,
    headers: {
        'Content-Type': 'application/json'
    },
    paramsSerializer: function (params) {
        return qs.stringify(params, {
            encode: false,
        });
    },
})

const logRequest = (config: AxiosRequestConfig<any>) => {
    if (process.env.REACT_APP_ALLOW_LOG_HTTP === "true") {
        console.log(`Thông tin request [${config.method}]:[${config.url}]:`, config);
    }
};

const logResponse = (response: AxiosResponse) => {
    if (process.env.REACT_APP_ALLOW_LOG_HTTP === "true") {
        console.log(`Thông tin response [${response.config.method}]:[${response.config.url}]:`, response);
    }
};

http.interceptors.request.use(
    function (config: AxiosRequestConfig<any>) {
        const token = clientStorage.get(ACCESS_TOKEN_KEY);
        if (token && config.headers) {
            config.headers[AUTHORIZATION_KEY] = `${TOKEN_TYPE_KEY} ${token}`;
        }
        logRequest(config);
        return config;
    },
    function (error) {
        return Promise.reject(error);
    }
);

http.interceptors.response.use(
    response => {
        logResponse(response);
        return response;
    },
    async error => {
        //refresh token
        if (error.response?.status === 401) {
            const refreshToken = clientStorage.get(REFRESH_TOKEN_KEY)
            const { data } = await http.get<ApiResult<JwtResultModel>>(`/auth/${refreshToken}`);
            http.defaults.headers.common[AUTHORIZATION_KEY] = `${TOKEN_TYPE_KEY} ${data.data.accessToken}`;
            clientStorage.set(ACCESS_TOKEN_KEY, data.data.accessToken);
            clientStorage.set(REFRESH_TOKEN_KEY, data.data.refreshToken);
            return http.request(error.config);
        }
        const _response = error.response?.data || { code: 500 }
        return Promise.reject(_response);
    }
);

export default http;

export {
    ApiResult,
};


