import S18Icon from '@core/components/Icon';
import { isExistInArray } from '@core/utils/check';
import { HEADER_OVERLOAD_KEY } from '@layouts/DefaultLayout/Components/HeaderConstant';
import ProfileComponent from '@layouts/DefaultLayout/Components/ProfileComponent';
import menuItems from '@layouts/DefaultLayout/menuItems';
import { IconButton } from '@mui/material';
import { useAppDispatch, useAppSelector } from '@stores/hook';
import { pushOverloadKey, removeOverloadKey, setShowSidebar } from '@stores/uISlice';
import { useMemo } from 'react';
import { Link } from 'react-router-dom';
import HorizontalMenuComponent from '../HorizontalMenuComponent';
import './HeaderComponent.scss';

export default function HeaderComponent() {
  const currentSession = useAppSelector(state => state.session.currentSession);
  const dispatch = useAppDispatch();
  const handlebtnShowMenuSidebar = () => {
    dispatch(setShowSidebar(true));
    dispatch(pushOverloadKey(HEADER_OVERLOAD_KEY));
    document.getElementById('bg-overlay')?.addEventListener('click', () => {
      dispatch(removeOverloadKey(HEADER_OVERLOAD_KEY));
      dispatch(setShowSidebar(false));
    }, { once: true })
  }
  const userMenus = useMemo(() => {
    const parents = menuItems
      .filter(x => (!x.roles || isExistInArray(x.roles, currentSession?.roles)) && isExistInArray(x.permissions, currentSession?.permissions));

    parents.forEach(menu => {
      if (menu.children?.length !== 0) {
        menu.children = menu.children?.filter(x => (!x.roles || isExistInArray(x.roles, currentSession?.roles)) && isExistInArray(x.permissions, currentSession?.permissions));
      }
    });
    return parents;
  }, [currentSession])

  return (
    <div className='header mui-fixed'>
      <div className='header-container'>
        <div className="header-logo flex items-center">
          <Link to='/'>
            <img src="/images/logo-vingroup.png" alt='logo' style={{ height: 32 }} />
          </Link>
          <IconButton className='2xl:hidden' onClick={handlebtnShowMenuSidebar}>
            <S18Icon className='text-white' type='outlined'>
              menu
            </S18Icon>
          </IconButton>
        </div>

        <div className='header-body w-full hidden 2xl:flex justify-center'>
          {
            userMenus.map(item => <HorizontalMenuComponent {...item} key={item.name} />)
          }
        </div>

        <div className="header-profile flex items-center">
          <ProfileComponent />
        </div>
      </div>
    </div >
  )
}
