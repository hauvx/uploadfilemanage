import S18Icon from '@core/components/Icon'
import { MenuItemComponentProps } from '@layouts/DefaultLayout/menuItems'
import { Link } from 'react-router-dom'
import './HorizontalMenuComponent.scss'

export default function HorizontalMenuComponent({ name, to, icon, children }: MenuItemComponentProps) {
  const hideChild = (event: React.MouseEvent) => {
    (event.target as Element).closest('.menu-dropdown')?.classList.add('hide-child');
    setTimeout(() => {
      (event.target as Element).closest('.menu-dropdown')?.classList.remove('hide-child')
    }, 1000);

  }
  if (!children || children.length === 0)
    return (
      <Link to={to || ''} onClick={hideChild} className="flex items-center px-4 text-white">
        <S18Icon className='mr-2' type={icon?.type} >{icon?.name}</S18Icon>
        <span className='whitespace-nowrap' >{name}</span>
      </Link>
    )

  return (
    <div className='menu-dropdown'>
      <div className='title flex items-center px-4'>
        <Link to={to || ''} className="flex items-center px-4 text-white">
          <S18Icon className='mr-2' type={icon?.type}>{icon?.name}</S18Icon>
          <span className='whitespace-nowrap'>{name}</span>
        </Link>
      </div>
      <div className='menu-dropdown-content absolute py-3 bg-white red menuItemCustom'>
        <div className="items">
          {
            children.map(child => <HorizontalMenuComponent {...child} key={child.name} />)
          }
        </div>
      </div>
    </div>
  )
}
