import S18Icon from '@core/components/Icon'
import '@layouts/DefaultLayout/Components/MenuItemComponent/MenuItemComponent.scss'
import { MenuItemComponentProps } from '@layouts/DefaultLayout/menuItems'
import AdjustOutlinedIcon from '@mui/icons-material/AdjustOutlined'
import ExpandMoreOutlinedIcon from '@mui/icons-material/ExpandMoreOutlined'
import { useState } from 'react'
import { NavLink } from 'react-router-dom'


export default function MenuItemComponent(props: MenuItemComponentProps) {

  const [isExpand, setIsExpand] = useState(false);
  const toggleExpand = () => {
    setIsExpand(!isExpand);
  }
  if (!props.children || props.children.length === 0) {
    return (
      <div className='menu-item flex items-center'>
        <div className='flex items-center w-full'>
          <NavLink to={props.to || ''} className='link-title'>
            {
              props.icon?.name ?
                <S18Icon className='mr-2' type='outlined'>
                  {props.icon?.name}
                </S18Icon> :
                <AdjustOutlinedIcon className='mr-2 text-xs' />
            }
            <span>{props.name}</span>
          </NavLink>
        </div>
      </div>
    )
  }
  return (
    <div className='menu-item flex items-center'>
      <div className='flex items-center w-full'>
        <div className={`dropdown w-full ${isExpand ? 'expand' : ''}`}>
          <div
            className="dropdown-title px-2 flex items-center justify-between"
            onClick={toggleExpand}>
            <div className='dropdown-title-name flex items-center'>
              {
                props.icon?.name ?
                  <S18Icon className='mr-2' type='outlined'>
                    {props.icon?.name}
                  </S18Icon> :
                  <AdjustOutlinedIcon fontSize='small' className='mr-2' />
              }
              <span>{props.name}</span>
            </div>
            <div className="dropdown-title-action">
              <ExpandMoreOutlinedIcon className='expand-icon' />
            </div>
          </div>

          <div className="dropdown-content pl-7">
            {props.children.map((item, index) => <MenuItemComponent key={index} {...item} />)}
          </div>

        </div>
      </div>
    </div>
  )
}
