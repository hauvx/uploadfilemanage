import S18Icon from '@core/components/Icon'
import { isExistInArray } from '@core/utils/check'
import { HEADER_OVERLOAD_KEY } from '@layouts/DefaultLayout/Components/HeaderConstant'
import MenuItemComponent from '@layouts/DefaultLayout/Components/MenuItemComponent'
import menuItems from '@layouts/DefaultLayout/menuItems'
import { IconButton } from '@mui/material'
import { useAppDispatch, useAppSelector } from '@stores/hook'
import { removeOverloadKey, setShowSidebar } from '@stores/uISlice'
import React, { useMemo } from 'react'
import { Link } from 'react-router-dom'
import './SidebarComponent.scss'

export default function SidebarComponent() {
  const dispatch = useAppDispatch();
  const currentSession = useAppSelector(state => state.session.currentSession);
  const userMenus = useMemo(() => {
    const parents = menuItems
      .filter(x => (!x.roles || isExistInArray(x.roles, currentSession?.roles)) && isExistInArray(x.permissions, currentSession?.permissions));

    parents.forEach(menu => {
      if (menu.children?.length !== 0) {
        menu.children = menu.children?.filter(x => (!x.roles || isExistInArray(x.roles, currentSession?.roles)) && isExistInArray(x.permissions, currentSession?.permissions));
      }
    });
    return parents;
  }, [currentSession])

  const isShow = useAppSelector(state => state.ui.isShowSidebar);
  const handlebtnHideMenuSidebar = () => {
    dispatch(removeOverloadKey(HEADER_OVERLOAD_KEY));
    dispatch(setShowSidebar(false));
  }
  return (
    <React.Fragment>
      <div className={`sidebar ${isShow ? 'show' : ''}`}>
        <div className="sidebar-header">
          <Link to='/' className='text-gray-900'>
            <img src="/images/logo.png" alt='logo' style={{ height: 32 }} />
          </Link>
          <IconButton onClick={handlebtnHideMenuSidebar}>
            <S18Icon className='text-white' type='outlined'>
              menu_open
            </S18Icon>
          </IconButton>
        </div>
        <div className="sidebar-content">
          {userMenus.map((item, index) => <MenuItemComponent key={index} {...item} />)}
        </div>
      </div>
    </React.Fragment>
  )
}
