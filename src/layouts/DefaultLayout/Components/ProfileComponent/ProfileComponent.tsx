import useLoading from '@core/hooks/useLoading';
import { REFRESH_TOKEN_KEY } from '@core/utils/appConstant';
import clientStorage from '@core/utils/clientStorage';
import getAvatarName from '@core/utils/getUserAvatar';
import Logout from '@mui/icons-material/Logout';
import Avatar from '@mui/material/Avatar';
import Divider from '@mui/material/Divider';
import IconButton from '@mui/material/IconButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import Tooltip from '@mui/material/Tooltip';
import authService from '@services/auth';
import { useAppDispatch, useAppSelector } from '@stores/hook';
import { logout } from '@stores/sessionSlice';
import qs from 'qs';
import * as React from 'react';

const LOGOUT_KEY_LOADING = 'LOGOUT_KEY_LOADING';

export default function ProfileComponent() {
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);
  const userInfo = useAppSelector(state => state.session?.userName);
  const callbackUri = window.location.pathname;
  const dispatch = useAppDispatch();
  const loading = useLoading();
  const handleLogout = () => {
    loading.show(LOGOUT_KEY_LOADING);
    authService
      .logout(clientStorage.get(REFRESH_TOKEN_KEY))
      .then(() => {
        dispatch(logout());

      })
      .finally(() => {
        const objNavigate = {} as { [key: string]: any };
        if (callbackUri && callbackUri !== '/' && !callbackUri.match(/^(\/)?auth(\/)?/))
          objNavigate.callbackUri = callbackUri;

        const queryParamsString = qs.stringify(objNavigate);
        loading.resolve(LOGOUT_KEY_LOADING);
        window.location.href = `/auth?${queryParamsString}`;
      })
  }

  const avatarName = getAvatarName(userInfo, userInfo)

  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  return (
    <React.Fragment>
      <Tooltip title="Thông tin tài khoản">
        <IconButton
          onClick={handleClick}
          size="small"
          sx={{ ml: 2 }}
          aria-controls={open ? 'account-menu' : undefined}
          aria-haspopup="true"
          aria-expanded={open ? 'true' : undefined}
        >
          <Avatar className='p-4 text-sm' sizes='small' sx={{ width: 8, height: 8 }} >
            {avatarName}
          </Avatar>
        </IconButton>
      </Tooltip>
      <Menu
        anchorEl={anchorEl}
        id="account-menu"
        open={open}
        onClose={handleClose}
        onClick={handleClose}
        PaperProps={{
          elevation: 0,
          sx: {
            overflow: 'visible',
            filter: 'drop-shadow(0px 2px 8px rgba(0,0,0,0.32))',
            mt: 1.5,
            '& .MuiAvatar-root': {
              width: 32,
              height: 32,
              ml: -0.5,
              mr: 1,
            },
            '&:before': {
              content: '""',
              display: 'block',
              position: 'absolute',
              top: 0,
              right: 14,
              width: 10,
              height: 10,
              bgcolor: 'background.paper',
              transform: 'translateY(-50%) rotate(45deg)',
              zIndex: 0,
            },
          },
        }}
        transformOrigin={{ horizontal: 'right', vertical: 'top' }}
        anchorOrigin={{ horizontal: 'right', vertical: 'bottom' }}
      >
        <MenuItem>
          <div className='flex items-center'>
            <Avatar sizes='small' className='p-5'>{avatarName}</Avatar>
            <div>
              <p>{userInfo}</p>
              <p className='text-xs'>{userInfo}</p>
            </div>
          </div>
        </MenuItem>
        <Divider />
        {/* <MenuItem>
          <ListItemIcon>
            <AccountCircleOutlinedIcon fontSize="small" />
          </ListItemIcon>
          Profile
        </MenuItem> */}
        <MenuItem onClick={handleLogout}>
          <ListItemIcon>
            <Logout fontSize="small" />
          </ListItemIcon>
          Logout
        </MenuItem>
      </Menu>
    </React.Fragment>

  )
}
