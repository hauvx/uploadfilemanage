import { useCallback, useEffect, useRef, useState } from "react";
import "./main.css";
import { useDropzone } from "react-dropzone";
import useNotification from "@core/hooks/useNotification";

export default function FileUploader({ onchangeFile ,isSuccess}: any) {
  const [fileName, setFileName] = useState("");
  const [mb, setMb] = useState<string>("");
  const fileInputRef = useRef<HTMLInputElement>(null);
  const notification = useNotification();
  const handleIconClick = () => {
    if (fileInputRef.current) {
      fileInputRef.current.click();
    }
  };

  const handleFileChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    var file = event.target.files?.[0];
    setOnchangeFile(file);
  };

  const setOnchangeFile = (file: any) => {
    if (file) {
      if(file.size > 5242880){
        notification('File ' + file.name + ' có dung lượng lớn hơn 5MB', {
          severity: "error",
        });
        return;
      }
      setFileName(file.name ? file.name : "");
      setMb(getSizeFile(file.size));
      // Xử lý tệp tin đã chọn ở đây
      onchangeFile(file);
    }
    else
    onchangeFile(null);
  };
  useEffect(() => {
    if(isSuccess){
      setFileName('');
      setOnchangeFile(null)
    }
  }, [isSuccess])
  const getSizeFile = (size: number): string => {
    if (size < 1024) return size + "bytes";
    size = size / 1024;
    if (size < 1024) return `${Math.round(size)} KB`;
    size = size / 1024;
    if (size < 1024) return `${Math.round(size)} MB`;
    size = size / 1024;
    return `${Math.round(size)} GB`;
  };
  const onDrop = useCallback((acceptedFiles: any) => {
    // Handle the dropped file here
    setOnchangeFile(acceptedFiles[0]);
  }, []);

  const { getRootProps, getInputProps, isDragActive } = useDropzone({ onDrop,accept: '.xlsx' });

  return (
    <div>
      <div className="leftSideFlieUpload dropzone" {...getRootProps()}>
        <input
          style={{ display: "none" }}
          ref={fileInputRef}
          onChange={handleFileChange}
          type="file"
          name="file"
          accept=".xlsx"
          className="upload-file"
          {...getInputProps()}
        />
        <div className={`controlUploader ${isDragActive ? "dropActive" : ""}`} onClick={handleIconClick}>
          <i className="fa fa-upload" style={{ fontSize: "25px", zIndex: "999999" }}></i>
          <span className="text-[15px]">Upload Files</span>
        </div>
      </div>
      {fileName && (
        <div className="rightSideFlieUpload text-[black]">
          Tên file: {fileName} {mb}
        </div>
      )}
    </div>
  );
}
