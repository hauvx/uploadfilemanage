import React from 'react'
import { Outlet } from 'react-router-dom'
import './DefaultLayout.scss'
import HeaderComponent from './Components/HeaderComponent'
import SidebarComponent from './Components/SidebarComponent'

declare type DefaultLayoutProps = {
  children?: React.ReactNode
}

export default function DefaultLayout({ children }: DefaultLayoutProps) {
  return (
    <React.Fragment>
      <HeaderComponent />
      <SidebarComponent />
      <main>
        <Outlet />
      </main>
    </React.Fragment>
  )
}
