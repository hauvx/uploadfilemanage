import { FontIconType } from "@core/components/Icon";

export declare type MenuItemComponentProps = {
    name: string,
    to?: string,
    icon?: {
        name: string,
        type?: FontIconType
    },
    children?: MenuItemComponentProps[],
    roles?: string[],
    permissions?: string[],
    description?: string;
}

const menuItems: MenuItemComponentProps[] = [
    {
        name: 'Trang chủ',
        to: '/',
        icon: {
            name: 'home',
            type: 'outlined'
        }
    },
    {
        name: 'Quản lý tempalte',
        to: '/manage-template-excel',
        icon: {
            name: 'upload_file',
            type: 'outlined'
        },
    }
]


export default menuItems;
