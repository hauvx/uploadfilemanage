import { createSlice, PayloadAction } from "@reduxjs/toolkit"

export interface ConfigsState {
    appConfig: {
        [key: string]: any
    }
}

const initialState: ConfigsState = {
    appConfig: {
        isDevelopmentMode: false
    }
}

export const configsSlice = createSlice({
    name: 'configs',
    initialState,
    reducers: {
        setAppConfigs: (state, action: PayloadAction<{[key: string]: any}>) => {
            state.appConfig = {
                ...state.appConfig,
                ...action.payload
            };
        },
    }
})

export const {
    setAppConfigs,
} = configsSlice.actions;

export default configsSlice.reducer



