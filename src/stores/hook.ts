import { TypedUseSelectorHook, useDispatch, useSelector } from "react-redux";
import { AppDispatch, AppState } from ".";

//Sử dụng useAppDispatch thay cho useDispatch
export const useAppDispatch = () => useDispatch<AppDispatch>();

//Sử dụng useAppSelector thay cho useSelector
export const useAppSelector: TypedUseSelectorHook<AppState> = useSelector;
