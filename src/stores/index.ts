import { Action, configureStore, ThunkAction } from '@reduxjs/toolkit';
import uIReducer from '@stores/uISlice';
import sessionReducer from '@stores/sessionSlice'

export const store = configureStore({
    reducer: {
        ui: uIReducer,
        session: sessionReducer
    },
    middleware: getDefaultMiddleware => getDefaultMiddleware({
        serializableCheck: {
            ignoredActions: [
                'ui/pushConfirm',
                'ui/removeConfirm',
                'ui/pushDialog',
                'ui/pushDialog'
            ],
            ignoredPaths: [
                'ui.confirmInfos',
                'ui.dialogInfos'
            ]
        }
    })
});


export type AppState = ReturnType<typeof store.getState>;

export type AppDispatch = typeof store.dispatch;

export type AppThunk<ReturnType = void> = ThunkAction<
    ReturnType,
    AppState,
    unknown,
    Action<string>
>
