import { createSlice } from '@reduxjs/toolkit';
import type { PayloadAction } from '@reduxjs/toolkit';
import React from 'react';
import { AlertColor } from '@mui/material';

export interface Notification {
    refId: string | number,
    children?: any,
    severity?: AlertColor,
}

export interface UIState {
    confirmInfos: ModelInfo[],
    dialogInfos: ModelInfo[],
    isShowSidebar: boolean,
    overloadKeys: string[],
    appConfig: {
        [key: string]: any
    },
    notifications: Notification[],
    loadingKeys: string[],
}

export interface ModelInfo {
    refId: string | number;
    element: React.ReactNode
}

const initialState: UIState = {
    confirmInfos: [],
    dialogInfos: [],
    isShowSidebar: false,
    overloadKeys: [],
    notifications: [],
    appConfig: {
        isDevelopmentMode: false
    },
    loadingKeys: []
}

export const uiSlice = createSlice({
    name: 'ui',
    initialState,
    reducers: {
        pushConfirm: (state, action: PayloadAction<ModelInfo>) => {
            state.confirmInfos.push(action.payload);
            state.confirmInfos = [...state.confirmInfos]
        },
        removeConfirm: (state, action: PayloadAction<string | number>) => {
            state.confirmInfos = state.confirmInfos.filter(x => x.refId !== action.payload);
        },
        pushDialog: (state, action: PayloadAction<ModelInfo>) => {
            state.dialogInfos.push(action.payload);
            state.dialogInfos = [...state.dialogInfos]
        },
        removeDialog: (state, action: PayloadAction<string | number>) => {
            state.dialogInfos = state.dialogInfos.filter(x => x.refId !== action.payload);
        },
        toggleShowSidebar: (state) => {
            state.isShowSidebar = !state.isShowSidebar;
        },
        setShowSidebar: (state, action: PayloadAction<boolean>) => {
            state.isShowSidebar = !state.isShowSidebar;
        },
        pushOverloadKey: (state, action: PayloadAction<string>) => {
            state.overloadKeys.push(action.payload);
        },
        removeOverloadKey: (state, action: PayloadAction<string>) => {
            state.overloadKeys = state.overloadKeys.filter(x => x !== action.payload);
        },
        setAppConfigs: (state, action: PayloadAction<{[key: string]: any}>) => {
            state.appConfig = {
                ...state.appConfig,
                ...action.payload
            };
        },
        queueNotification: (state, action: PayloadAction<Notification>) => {
            state.notifications.push(action.payload);
        },
        removeNotification: (state, action: PayloadAction<number | string>) => {
            state.notifications = state.notifications.filter(noti => noti.refId !== action.payload)
        },
        pushLoading: (state, action: PayloadAction<string>)=>{
            state.loadingKeys = [...state.loadingKeys, action.payload]
        },
        resolveLoading: (state, action: PayloadAction<string>)=>{
            state.loadingKeys = state.loadingKeys.filter(key=> key !== action.payload)
        }
    }
})

export const {
    pushConfirm,
    removeConfirm,
    pushDialog,
    removeDialog,
    setShowSidebar,
    pushOverloadKey,
    removeOverloadKey,
    setAppConfigs,
    queueNotification,
    removeNotification,
    pushLoading,
    resolveLoading
} = uiSlice.actions;

export default uiSlice.reducer
