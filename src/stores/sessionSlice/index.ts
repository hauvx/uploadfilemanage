import http from "@core/http"
import { ACCESS_TOKEN_KEY, AUTHORIZATION_KEY, REFRESH_TOKEN_KEY } from "@core/utils/appConstant"
import clientStorage from "@core/utils/clientStorage"
import { createAsyncThunk, createSlice, PayloadAction } from "@reduxjs/toolkit"
import userService, { User } from "@services/user"
import { AppState } from ".."

declare type CurrentSesstion = {
    userInfo?: User,
    roles?: string[],
    permissions?: string[],
    accessToken?: string,
    reFreshToken?: string;
    userName?: string;
}

export declare type SessionState = {
    isLogin: boolean,
    currentSession?: CurrentSesstion,
    isCheckedLogin: boolean;
    userName?: string;
}

const initialState: SessionState = {
    isLogin: false,
    isCheckedLogin: false
}

export const fetchUserAsync = createAsyncThunk(
    'session/fetchUser',
    async () => {
        const { data } = await userService.me();
        return {
            userInfo: data.userInfo
        }
    }
)


export const sessionSlice = createSlice({
    name: 'session',
    initialState,
    reducers: {
        setSession: (state, action: PayloadAction<SessionState>) => {
            state.isLogin = action.payload.isLogin;
            state.currentSession = action.payload.currentSession;
            state.userName = action.payload.userName;
        },
        logout: (state) => {
            state.isLogin = false;
            state.currentSession = undefined;
            http.defaults.headers.common[AUTHORIZATION_KEY] = '';
            clientStorage.remove(ACCESS_TOKEN_KEY);
            clientStorage.remove(REFRESH_TOKEN_KEY);
        },
        checkedLogin: (state) => {
            state.isCheckedLogin = true;
        }
    },
    extraReducers: (builder) => {
        //Build GET USER INFO
        builder
            .addCase(fetchUserAsync.pending, (state) => {

            })
            .addCase(fetchUserAsync.fulfilled, (state, action) => {
                state.isLogin = true;
                state.currentSession = action.payload
            })
            .addCase(fetchUserAsync.rejected, (state, action) => {
                // console.log('Login thất bại', state, action);
            });
    },
});

export default sessionSlice.reducer;
export const { logout, checkedLogin ,setSession} = sessionSlice.actions

export const selectPermissionName = (state: AppState, permissionName: string) =>
    state.session.currentSession?.permissions?.includes(permissionName)
