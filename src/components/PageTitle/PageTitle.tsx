
import { Breadcrumbs } from '@mui/material';
import './PageTitle.scss';

declare type PageTitleProps = {
  children?: React.ReactNode,
  pageName?: string
}

export default function PageTitle({ children, pageName }: PageTitleProps) {
  return (
    <div className='flex items-center justify-between page-title'>
      <h3 className='title'>
        {pageName}
      </h3>
      <Breadcrumbs maxItems={5} className='hidden md:block' aria-label="breadcrumb">
        {children}
      </Breadcrumbs>
    </div>
  )
}
