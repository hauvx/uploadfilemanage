import ColumnFilter from '@components/ColumnFilter';
import { Sort } from '@core/models/Sort';
import ImportExportOutlinedIcon from '@mui/icons-material/ImportExportOutlined';
import UpgradeOutlinedIcon from '@mui/icons-material/UpgradeOutlined';
import { Tooltip } from '@mui/material';
import { useEffect, useState } from 'react';
import './style.scss';

function RenderSortIcon({ isSorted, isSortDesc }: { isSorted: boolean, isSortDesc: boolean }) {
  if (isSorted)
    return <UpgradeOutlinedIcon className={`sort-arrow ${isSortDesc ? 'down' : 'up'}`} />;

  return <ImportExportOutlinedIcon sx={{ opacity: .2 }} />;
}

export default function TableCellContent({
  column,
  sorts, setSorts,
  searchCondition, setSearchCondition,
  filters, setFilters,setPage }: any) {

  const [isSorted, setIsSorted] = useState(false);

    //Mặc định sẽ giảm dần
  const [isSortDesc, setIsSortDesc] = useState(true);

  const [tooltip, setTooltip] = useState('Dữ liệu chưa được sắp xếp theo cột này, click để sắp xếp theo chiều giảm dần');

  const handleClickSort = () => {

    //Nếu đang sort
    if (isSorted) {
      //Nếu đang sort giảm dần => chuyển qua tăng dần
      if (isSortDesc) {
        const _sort: Sort = {
          orderBy: column.id,
          isSortDesc: false
        }
        const _newSorts = _sort;


        setSearchCondition({
          ...searchCondition,
          s: _newSorts
        })
        setSorts(_newSorts);
        setIsSortDesc(false);
        setTooltip('Dữ liệu đang được sắp xếp theo chiều tăng dần với cột này, click để hủy sắp xếp đối với cột này');
        return;
      }
      setIsSorted(false);

      setSorts(undefined);
      setSearchCondition({
        ...searchCondition,
        s: undefined
      })
      setTooltip('Dữ liệu chưa được sắp xếp theo cột này, click để sắp xếp theo chiều giảm dần');
      return;
    }
    //Nếu chưa sort
    const sort: Sort = {
      orderBy: column.id,
      isSortDesc: true
    }
    const newSorts = sort;
    setSorts(newSorts);
    setIsSorted(true);//set trạng thái là đang sort
    setSearchCondition({
      ...searchCondition,
      s: newSorts
    })
    setIsSortDesc(true);//set sort tăng
    setTooltip('Dữ liệu đang được sắp xếp theo chiều giảm dần với cột này, click để sắp xếp cột này theo chiều tăng dẫn');
  }

  useEffect(() => {
    setIsSortDesc(false);
    setIsSorted(false);
    const colSort = sorts
    if (colSort?.orderBy === column.id) {
      setIsSorted(true);
      setIsSortDesc(colSort?.isSortDesc === true);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  },[sorts])


  return (
    <div className="flex items-center justify-between">
      {
        column.allowSort ?
          <div className='cursor-pointer w-full' onClick={handleClickSort}>
            <Tooltip title={tooltip}>
              <p className='flex items-center justify-between'>
                {column.label}
                <RenderSortIcon isSorted={isSorted} isSortDesc={isSortDesc} />
              </p>
            </Tooltip>
          </div> :
          <p>{column.label}</p>
      }
      {
        column.allowFilter && <ColumnFilter
          filters={filters}
          setFilters={setFilters}
          searchCondition={searchCondition}
          setSearchCondition={setSearchCondition}
          column={column}
          setPage={setPage}
        />
      }
    </div>
  )
}
