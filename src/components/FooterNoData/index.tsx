import { TableCell, TableRow } from '@mui/material';
import React from 'react';

export default function FooterNoData({
  columnsCurrencyLenght
}: any) {
  return (
    <React.Fragment>
       <TableRow><TableCell  className="footer-no-data" colSpan={columnsCurrencyLenght} align="center" >Không có dữ liệu</TableCell></TableRow>
    </React.Fragment>
  )
}
