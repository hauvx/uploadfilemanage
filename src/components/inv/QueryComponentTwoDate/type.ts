import { BaseTextFieldProps, PopperPlacementType } from "@mui/material";

export default interface BaseDepartmentTreeProps
{
    position?: string,
    placeholder?: string,
    size?: BaseTextFieldProps["size"],
    placement?: PopperPlacementType,
    value?: string[]
}
