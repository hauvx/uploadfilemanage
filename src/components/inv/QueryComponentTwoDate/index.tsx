import useLoading from "@core/hooks/useLoading";
import objectHelper from "@core/utils/objectHelper";
import { ArrowBack, CloudUpload } from "@mui/icons-material";
import HighlightOffIcon from "@mui/icons-material/HighlightOff";
import SearchOutlinedIcon from "@mui/icons-material/SearchOutlined";
import {
  Button,
  Divider,
  FormControl,
  FormHelperText,
  IconButton,
  InputAdornment,
  Paper,
  TextField
} from "@mui/material";
import { DatePicker } from "@mui/x-date-pickers";
import { useFormik } from "formik";
import moment from "moment";
import { useState } from "react";
import * as yup from "yup";

const minDate = new Date("1990/01/01");
const maxDate = moment().toDate();

const validationSchema = yup.object({
  fromDate: yup
    .date()
    // .default(moment().toDate())
    .required("Ngày tháng không được để trống!")
    .nullable()
    .min(minDate, "Ngày phải lớn hơn 01/01/1990")
    .max(maxDate, "Ngày phải nhỏ hơn " + moment(maxDate.toString()).format("DD/MM/yy"))
    .typeError("Vui lòng nhập theo định dạng dd/mm/yy"),
  toDate: yup
    .date()
    // .default(moment().toDate())
    .required("Ngày tháng không được để trống!")
    .nullable()
    .min(minDate, "Ngày phải lớn hơn 01/01/1990")
    .max(maxDate, "Ngày phải nhỏ hơn " + moment(maxDate.toString()).format("DD/MM/yy"))
    .typeError("Vui lòng nhập theo định dạng dd/mm/yy"),
});

export default function InvQueryComponentTwoDate({
  searchCondition,
  setSearchCondition,
  handleBackTemplate,
  setPage,
  handleShowTempalte,
  showUploadFile
}: any) {
  const [messFromDate, setMessFromDate] = useState("");
  const [messToDate, setMessToDate] = useState("");
  const [isError, setIsError] = useState(false);
  const loading = useLoading();
  const [keyWord, setKeyWord] = useState('')
  //Form search
  const searchForm = useFormik({
    initialValues: {
      fileName: "",
      fromDate: maxDate,
      toDate: maxDate,
    },
    validationSchema: validationSchema,
    onSubmit: async ({ fileName, fromDate, toDate }) => {
      if (!searchForm.isValid) return;
      const newSearchCondition = {
        ...searchCondition,
        p: {
          page: 1,
        },
        q: objectHelper.filterNonNull({
          fileName: keyWord,
          fromDate: fromDate ? fromDate : null,
          toDate: toDate ? toDate : null,
        }),
      };
      setPage(0);
      setSearchCondition(newSearchCondition);
    },
  });

  const handleDownloadFile = (file: string) => {
    handleBackTemplate &&
      handleBackTemplate();
  };

  return (
    <Paper sx={{ width: "100%", marginTop: 2 }} className="pt-2">
      <form className="search-form">
        <div className="controls grid p-3 grid-cols-1 md:grid-cols-3 xl:grid-cols-4 gap-4">
          <FormControl>
            <TextField
              variant="outlined"
              id="standard-basic"
              label="Nhập từ khóa tìm kiếm"
              placeholder="Tìm theo Code hoặc Name"
              size="small"
              value={keyWord}
              onChange={(e) => setKeyWord(e.target.value)}
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    {keyWord && (
                      <IconButton
                        title="Xóa hết"
                        aria-label="toggle password visibility"
                        edge="end"
                        onClick={() => setKeyWord('')}
                      >
                        <HighlightOffIcon />
                      </IconButton>
                    )}
                  </InputAdornment>
                ),
              }}
            />
          </FormControl>
          <FormControl>
            <DatePicker
              disableFuture
              label="From Date"
              inputFormat="DD/MM/YY"
              value={searchForm.values.fromDate}
              minDate={minDate}
              maxDate={
                searchForm.values.toDate !== null &&
                searchForm.values.toDate !== undefined &&
                searchForm.values.toDate < maxDate
                  ? searchForm.values.toDate
                  : maxDate
              }
              onChange={(value) => {
                let valueFromDate = moment
                  .utc(value)
                  .add(new Date().getTimezoneOffset() / -60, "hours")
                  .toDate();

                searchForm.setFieldValue("fromDate", valueFromDate);
                if (searchForm.values.toDate && valueFromDate) {
                  let toDate = moment(searchForm.values.toDate).format("YYYY/MM/DD");
                  if (new Date(toDate) < new Date(moment(valueFromDate).format("YYYY/MM/DD"))) {
                    setIsError(true);
                    setMessFromDate(
                      "Ngày phải nhỏ hơn hoặc bằng " + moment(searchForm.values.toDate).format("DD/MM/YY")
                    );
                  } else {
                    setIsError(false);
                    setMessToDate("");
                    setMessFromDate("");
                  }
                }
              }}
              renderInput={(params) => (
                <FormControl fullWidth>
                  <TextField
                    required={true}
                    size="small"
                    error={Boolean(searchForm.touched.fromDate && searchForm.errors.fromDate)}
                    {...params}
                  />
                  <FormHelperText style={{ color: "red" }}>
                    {(searchForm.errors.fromDate as string) ?? messFromDate}
                  </FormHelperText>
                </FormControl>
              )}
            />
          </FormControl>
          <FormControl>
            <DatePicker
              disableFuture
              label="To Date"
              inputFormat="DD/MM/YY"
              value={searchForm.values.toDate}
              minDate={
                searchForm.values.fromDate != null && searchForm.values.fromDate !== undefined
                  ? searchForm.values.fromDate
                  : minDate
              }
              maxDate={maxDate}
              onChange={(value) => {
                let valueToDate = moment
                  .utc(value)
                  .add(new Date().getTimezoneOffset() / -60, "hours")
                  .toDate();
                searchForm.setFieldValue("toDate", valueToDate);

                if (searchForm.values.fromDate && valueToDate) {
                  let fromDate = moment(searchForm.values.fromDate).format("YYYY/MM/DD");
                  if (new Date(fromDate) > new Date(moment(valueToDate).format("YYYY/MM/DD"))) {
                    setIsError(true);
                    setMessToDate(
                      "Ngày phải lớn hơn hoặc bằng " + moment(searchForm.values.fromDate).format("DD/MM/YY")
                    );
                  } else {
                    setIsError(false);
                    setMessToDate("");
                    setMessFromDate("");
                  }
                }
              }}
              renderInput={(params) => (
                <FormControl fullWidth>
                  <TextField
                    size="small"
                    required={true}
                    error={Boolean(searchForm.touched.toDate && searchForm.errors.toDate)}
                    {...params}
                  />
                  <FormHelperText style={{ color: "red" }}>
                    {(searchForm.errors.toDate as string) ?? messToDate}
                  </FormHelperText>
                </FormControl>
              )}
            />
          </FormControl>
        </div>
        <Divider className="mt-2" />
        <div className="actions p-3 flex gap-3">
          <Button
            variant="contained"
            onClick={searchForm.submitForm}
            disabled={!searchForm.isValid || isError}
            startIcon={<SearchOutlinedIcon />}
          >
            Tìm kiếm
          </Button>
          <Button
            variant="contained"
            onClick={() => showUploadFile()}
            className="success"
            disabled={!searchForm.isValid || isError}
            startIcon={<CloudUpload />}
          >
            Upload Excel
          </Button>

          <Button
            variant="contained"
            onClick={() => handleShowTempalte("")}
            className="back"
            disabled={!searchForm.isValid || isError}
            startIcon={<ArrowBack />}
          >
            Quay lại
          </Button>
        </div>
      </form>
    </Paper>
  );
}
