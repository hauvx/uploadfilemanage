import AutorenewOutlinedIcon from '@mui/icons-material/AutorenewOutlined';
import { Button } from '@mui/material';
export default function SetupDevice() {
    return (
        <div className='w-screen h-screen flex justify-center items-center'>
            <div className='flex flex-col items-center justify-center'>
                <div className="image-wrap">
                    <img src='/images/unable-to-connect.png' alt='Không thể kết nối đến máy chủ' width={350} />
                </div>
                <h2 className='mt-6'>Không thể kết nối đến máy chủ</h2>
                <p className='mt-6'>
                    Không thể kết nối đến máy chủ, nếu bạn đang sử dụng proxy, vui lòng add proxy:
                    `<code className='bg-blue-200'>*.fpt.net</code>` sau đó thử lại hoặc liên hệ admin.
                </p>
                <div className='mt-6'>
                    <Button
                        startIcon={<AutorenewOutlinedIcon />}
                        onClick={()=>window.location.reload()}
                        variant="contained">
                        Kết nối lại
                    </Button>
                </div>
            </div>
        </div>
    )
}
