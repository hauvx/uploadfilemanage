import FilterAltOutlinedIcon from '@mui/icons-material/FilterAltOutlined';
import SearchIcon from '@mui/icons-material/Search';
import { ClickAwayListener, FormControl, FormHelperText, IconButton, Paper, Popper, TextField, Tooltip } from '@mui/material';
import { DatePicker } from '@mui/x-date-pickers';
import moment from 'moment';
import React, { useEffect, useState } from 'react';

export default function ColumnFilter({
  searchCondition,
  setSearchCondition,
  filters, setFilters,
  column, setPage
}: any) {
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const [searchKey, setSearchKey] = useState('');
  const [isFilted, setIsFilted] = useState(false);

  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(anchorEl ? null : event.currentTarget);
  };

  const open = Boolean(anchorEl);



  useEffect(() => {
    setIsFilted(false);
    setSearchKey('');
    const currentFilter = filters
    if (currentFilter?.name === column.id) {
      if (column.filterType === 'datetime')
        setSearchKey(currentFilter?.value.toString())
      else
        setSearchKey(currentFilter?.value === undefined ? '' : currentFilter?.value.trim());
      setIsFilted(true);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [filters]);

  const handleKeyDown = (e: any) => {
    if (e.key === "Enter") {
    }
  };

  return (
    <ClickAwayListener onClickAway={() => setAnchorEl(null)}>
      <div>
        <Tooltip title='Bổ sung điều kiện lọc' sx={{ opacity: isFilted ? 1 : .2, ":hover": { opacity: isFilted ? 1 : .5 } }}>
          <IconButton onClick={handleClick}>
            <FilterAltOutlinedIcon />
          </IconButton>
        </Tooltip>
        <Popper open={open} placement='bottom-end' anchorEl={anchorEl} sx={{ zIndex: 20000 }}>
          <Paper>
            <div className='flex items-center pl-4 pr-2 py-3'>
              <div className='w-full'>
                <ColumnFilterRender
                  filterType={column.filterType}
                  handleKeyDown={handleKeyDown}
                  setSearchKey={setSearchKey}
                  searchKey={searchKey}
                />
              </div>
              <div>
                <Tooltip title='Tìm theo điều kiện lọc'>
                  <IconButton >
                    <SearchIcon />
                  </IconButton>
                </Tooltip>
              </div>
            </div>
          </Paper>
        </Popper>
      </div>
    </ClickAwayListener>
  )
}


function ColumnFilterRender({ filterType, handleKeyDown, setSearchKey, searchKey }: any) {
  const [errorMessage, seterrorMessage] = useState('');

  if (filterType === 'datetime')
    return (
      <DatePicker
        disableFuture
        inputFormat='DD/MM/YY'
        value={searchKey}

        onChange={(value) => {
          if (!value) {
            setSearchKey(null);
            seterrorMessage('')
            return;
          }
          const utc = moment.utc(value);
          if (!utc.isValid())
            seterrorMessage('Nhập theo định dạng dd/mm/yy');
          else
            seterrorMessage('')
          let valueToDate = utc.add(new Date().getTimezoneOffset() / -60, 'hours').toDate();
          setSearchKey(valueToDate);
        }}
        renderInput={(params) => (
          <FormControl fullWidth>
            <TextField
              size='small'
              required={true}
              variant='standard'
              onKeyDown={handleKeyDown}
              onChange={e => {
                if (!e.target.value)
                  setSearchKey(null);
              }}
              error={Boolean(errorMessage)}
              {...params}
            />
            <FormHelperText style={{ color: "red", marginLeft:0 }}>
              {errorMessage}
            </FormHelperText>
          </FormControl>
        )}
      />
    )
  if (!filterType || filterType === 'string')
    return (
      <TextField
        onKeyDown={handleKeyDown}
        className='w-full'
        onChange={(e) => setSearchKey(e.target.value)}
        value={searchKey}
        autoComplete='off'
        placeholder='Nhập điều kiện lọc...'
        variant='standard'
        size='small' />
    )
  return <></>
}

