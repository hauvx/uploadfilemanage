import { SvgIconProps, SvgIcon } from "@mui/material";

export default function CsvExport(props: SvgIconProps) {
  return (
    <SvgIcon
      className="close"
      fontSize="inherit"
      {...props}
    >
      {/* tslint:disable-next-line: max-line-length */}
      <image width="24" height="24" x="0" y="0" opacity={.6}
    href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAQAAABKfvVzAAAABGdBTUEAALGPC/xhBQAAACBjSFJN
AAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QA/4ePzL8AAAAHdElN
RQfmCwQEBg81l+ayAAABAklEQVQ4y73TvUoDQRSG4WdjLkC0zKZR0MZW8AYC/mBp4xXYWNsIiZVe
hqUoFkKIjQj+tYK3YNJYSLDXjFU0s9ndxMZzqjln3pnvfMzwn7GlJ4xlqxjo5mwvRYKQWwtO/goU
IGXAD5JEzXg9rA0jgeoE55IsWBkxNJYQ55SGFkJB0FaT6ggOLdkxsODFs8UY+J1hz7mBXa+unHr0
oeHWl0axocOTbiw7FlzYtumySFLqwb26YF/fvDfv5szqa6vl3dCRqrsWNK1Y9ylYtSZIxyV1J7qT
kbRRihQ9m/xitlfxxxgFepmncaaKViSrVT7JKDLhu8ZxIAiaMDMV8CRx5wi+AZ2uyRZy/+AvAAAA
JXRFWHRkYXRlOmNyZWF0ZQAyMDIyLTExLTA0VDAzOjA2OjE1KzAxOjAw0oIMPgAAACV0RVh0ZGF0
ZTptb2RpZnkAMjAyMi0xMS0wNFQwMzowNjoxNSswMTowMKPftIIAAAAASUVORK5CYII=" />


    </SvgIcon>
  );
}
