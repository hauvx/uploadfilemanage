import { SvgIconProps, SvgIcon } from "@mui/material";

export default function ExcelExport(props: SvgIconProps) {
  return (
    <SvgIcon
      className="close"
      fontSize="inherit"
      {...props}
    >
      {/* tslint:disable-next-line: max-line-length */}
      <image width="24" height="24" x="0" y="0" opacity={.6}
        href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAQAAABKfvVzAAAABGdBTUEAALGPC/xhBQAAACBjSFJN
AAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QA/4ePzL8AAAAHdElN
RQfmCwQBOh4qH31UAAAA5klEQVQ4y72TrQ7CMBDHfxsjhDeYR5AQXoQhEfO8BJKRIBAYEjQawTOQ
IPAIDDg+PAgwfBXBx8radZvhanp3/V36v/bgf+axQ0RWYALU4wmIQGgigl5WIBaJB2IQEyAhlpSW
vU8ktHfOMfTN0qG25uAAQZU8GyaGcpIGlzNDmtyoxOqLBPscWTAyNCQSdLkjKOtyOg3gceFEXZJv
hVu1rUVWTLnSoMQhjeiAB1XK3OmqV9K9wxqfJeBTSNdWY84mo4XAnt/vNsYBgm91ZZxqbCPTJiOJ
AytbC4Gg/XJyKYA5FjM6L+cJCqCAKl80tdsAAAAldEVYdGRhdGU6Y3JlYXRlADIwMjItMTEtMDRU
MDE6NTg6MzArMDA6MDB14Wo7AAAAJXRFWHRkYXRlOm1vZGlmeQAyMDIyLTExLTA0VDAxOjU4OjMw
KzAwOjAwBLzShwAAAABJRU5ErkJggg==" />
    </SvgIcon>
  );
}
