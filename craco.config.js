const path = require(`path`);

module.exports = {
  eslint: {
    enable: false
  },
    webpack: {
        alias: {
            "@core": path.resolve(__dirname, "src/@core"),
            "@layouts": path.resolve(__dirname, "src/layouts"),
            "@components": path.resolve(__dirname, "src/components"),
            "@pages": path.resolve(__dirname, "src/pages"),
            "@stores": path.resolve(__dirname, "src/stores"),
            "@services": path.resolve(__dirname, "src/services"),
        },
    },
};
